@extends('layouts.fixed')

@section('title','Faculty Evaluation System')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Student</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fas fa-tachometer-alt"></i> Home</a></li>
            <li><a href="#">Student</a></li>

        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <button class="btn btn-primary"><a href="{{url('student-create')}}" style="color: white;"> Add Student</a></button>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="myTable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>SL NO</th>
                                    <th>Name</th>
                                    <th>Father's Name</th>
                                    <th>Mother's Name</th>
                                    <th>Date of Birth</th>
                                    <th>Department</th>
                                    <th>E-mail</th>
                                    <th>Phone</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $i = 1;?>
                            @foreach($students as $student)
                                <tr>
                                    <th> {{$i++}}</th>
                                    <td> {{$student->name}}</td>
                                    <td> {{$student->f_name}}</td>
                                    <td> {{$student->m_name}}</td>
                                    <td> {{$student->dob}}</td>
                                    <td> {{$student->department->name}}</td>
                                    {{--<td> {{$student->trimester->name}}</td>--}}
                                    <td> {{$student->email}}</td>
                                    <td> {{$student->phone}}</td>
                                    {{--<td> {{$student->password}}</td>--}}
                                    <td>
                                        <div class="row">
                                            <div class="col-md-3">

                                                <a href="{{url('student-edit',$student->id)}}" class="btn btn-info btn-sm"><i class="fa fa-pencil"></i> Edit</a>
                                            </div>
                                            <div class="col-md-2">
                                                    {{ Form::open(['url'=>['student-delete',$student->id],'method'=>'post']) }}
                                                        <input type="hidden" name="_method" value="DELETE">
                                                    {{ csrf_field()}}
                                                    <button type="submit" class="btn btn-danger btn-sm" ><i class="fa fa-trash-o"></i> Delete</button>
                                                {{Form::close()}}
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->

@stop

@section('script')

    <!-- DataTables -->
    <script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>

    <!-- page script -->
    <script>
        $(function () {
            $('#example1').DataTable();
            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : false,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })
        })
    </script>
@stop