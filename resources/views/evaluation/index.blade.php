@extends('layouts.fixed')

@section('title','Faculty Evaluation System')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Create Evaluation</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fas fa-tachometer-alt"></i> Home</a></li>
            <li><a href="#">Evaluation</a></li>

        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- SELECT2 EXAMPLE -->
        <div class="box box-default">
            <div class="box-header with-border">
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fas fa-minus"></i></button>
                    {{--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fas fa-times"></i></button>--}}
                </div>
                <br>
            </div>

            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class= "col-md-5 col-md-offset-4 control-label text-center" style="border: 1px solid yellow; color: yellow; background-color: red;">
                        <strong>Note :</strong> Once you submit with marks, it will never be changed.<br>
                        Highest mark is <strong>6</strong> and lowest mark is <strong>1</strong>.
                    </div>
                    {{ Form::open(['action' => 'EvaluationController@store','method'=>'post','id' => 'form']) }}
                    <div class="col-md-8 col-md-offset-2">
                        <div class="form-group">
                            {{ Form::label('Subject :') }}
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fas fa-calendar-alt"></i>
                                </div>
                                {{ Form::select('subject_id',$subjects,null,['class'=>'form-control select2','id'=>'subject_id','placeholder'=>'Select Subject'])}}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('Teacher Name:') }}
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fas fa-calendar-alt"></i>
                                </div>
                                {{ Form::text('teacher_name',null,['class'=>'form-control','id'=>'teacher_name'])}}
                                {{ Form::hidden('teacher_id',null,['class'=>'form-control','id'=>'teacher_id'])}}
                                {{ Form::hidden('trimester_id',null,['class'=>'form-control','id'=>'trimester_id'])}}
                            </div>
                        </div>






                </div>
                <!-- /.col -->
            </div>
            <!-- /.box-body -->
            <!-- /.table start -->
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Questions</h3>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive no-padding">
                            <table class="table table-hover" id="tableid">
                                <tr>
                                    <th>SN</th>
                                    <th>Title</th>
                                    <th>Question</th>
                                    <th>Marks</th>
                                </tr>
                                @php $i=0; @endphp
                                @foreach($questions as $question)
                                <tr>
                                    {{--<td>{{dd($question)}}</td>--}}
                                    <td>{{++$i}}</td>
                                    <td>{{$question->name}}</td>
                                    <td>{{$question->details}}</td>
                                    <td>
                                        {{Form::number('marks[]',null,['class'=>'form-control marks','required','placeholder'=>'type 1 to 6'])}}
                                        {{Form::hidden('question_id[]',$question->id,['class'=>'form-control'])}}

                                    </td>
                                </tr>
                                @endforeach
                            </table>
                        </div>
                        <!-- /.box-body -->

                        {{--submit start--}}

                        <div class="form-group row">
                            <div class="col-md-10">
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>
                    </div>

                    {{Form::close()}}
                        {{--submit end--}}

                    </div>
                    <!-- /.box -->
                </div>
            </div>
            {{--table end--}}
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->

@stop

@section('style')



@stop

@section('script')



    <!-- Page script -->
    <script>
        $('#subject_id').change(function () {
            var subject_id = $('#subject_id').val();
            var csrf = "{{csrf_token()}}";
            $.ajax({
                type:"post",
                url:"{{route('load_evaluation_teacher')}}",
                data: {subject_id:subject_id,_token:csrf},
                success:function(data) {
                    $("#teacher_id").val(data[0]);
                    $("#teacher_name").val(data[1]);
                    $("#trimester_id").val(data[2]);
                }
            })
        });
    </script>
    <script>
        $(".marks").on('keyup change',function() {
            var $row = $(this).closest("tr");    // Find the row
            var $text = $row.find(".marks").eq(0).val(); // Find the text

            if($text<1 || $text>6){
                $row.find(".marks").eq(0).val('');
            }
        });
    </script>

@stop