@extends('layouts.fixed')

@section('title','Faculty Evaluation System')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Overall Teacher Progression</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fas fa-tachometer-alt"></i> Home</a></li>
            <li><a href="#">Overall Teacher Progression</a></li>

        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- SELECT2 EXAMPLE -->
        <div class="box box-default">
            <div class="box-header with-border">
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fas fa-times"></i></button>
                </div>
                <br>
            </div>

            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    {{--my content goes here--}}
                    <div class="col-md-11 col-md-offset-1">
                        <h1><strong>{{$teacher->name}}</strong></h1>
                    </div>
                    <div class="col-md-12">
                        <div class="progress">
                            <div class="progress-bar progress-bar-striped active
                            @php
                                if($trimester_final_result<40){ echo "progress-bar-danger";}
                                elseif($trimester_final_result<60){ echo "progress-bar-warning";}
                                elseif($trimester_final_result<80){ echo "progress-bar-info";}
                                elseif($trimester_final_result<=100){ echo "progress-bar-success";}
                            @endphp"

                                 role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width:{{number_format($trimester_final_result,2)}}%">
                                {{number_format($trimester_final_result,1)}}%
                            </div>
                        </div>

                        <div class="col-md-5 col-md-offset-2">
                            {{ Form::label('Name') }}
                            <p>{{$teacher->name}}</p>
                        </div>

                        <div class="col-md-4 col-md-offset-1">
                            {{ Form::label('Department') }}
                            <p>{{$teacher->department->name}}</p>
                        </div>

                        <div class="col-md-5 col-md-offset-2">
                            {{ Form::label('E-Mail') }}
                            <p>{{$teacher->email}}</p>
                        </div>

                        <div class="col-md-4 col-md-offset-1">
                            {{ Form::label('Joining Date') }}
                            <p>{{$teacher->join_date}}</p>
                        </div>

                        <div class="col-md-4 col-md-offset-2">
                            {{ Form::label('Trimester') }}
                            <p>{{$trimester->name}}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <table class="table table-responsive">
                                <tr>
                                    <th>SN</th>
                                    <th>Subject</th>
                                    <th>Credit</th>
                                    <th>Percentage</th>
                                </tr>
                                    @php
                                        use App\Evaluation;
                                        $cum_avg =0;
                                        $i=0;
                                        $subjects = Evaluation::query()
                                            ->where('teacher_id',$teacher->id)
                                            ->where('trimester_id',$trimester->id)
                                            ->groupBy('subject_id')
                                            ->get();
                                        $subject_count = $subjects->count();
                                        foreach ($subjects as $subject) {
                                            $sum = Evaluation::query()
                                                ->where('subject_id',$subject->subject_id)
                                                ->where('teacher_id',$teacher->id)
                                                ->where('trimester_id',$trimester->id)
                                                //->get();
                                                ->sum('marks');
                                                //dd($sum);
                                            //dd($subject);
                                            $count = Evaluation::query()
                                                ->where('subject_id',$subject->subject_id)
                                                ->where('teacher_id',$teacher->id)
                                                ->where('trimester_id',$trimester->id)
                                                ->count();
                                                //dd($count);
                                                if($count>0){
                                                    $avg=$sum/$count;
                                                    $percentage = ($avg/6)*100;
                                                    $percentage = number_format($percentage,1);
                                                    $cum_avg+=$percentage;
                                                }

                                                $subject_name = \App\Subject::query()->findOrFail($subject->subject_id);
                                                echo "<tr>";
                                                echo "<td>".++$i."</td>";
                                                echo "<td>{$subject_name->name}</td>";
                                                echo "<td>{$subject_name->credit}</td>";
                                                echo "<td><div class='progress'><div class='progress-bar progress-bar-striped active ";

                                                if($percentage<40){ echo "progress-bar-danger'";}
                                                elseif($percentage<60){ echo "progress-bar-warning'";}
                                                elseif($percentage<80){ echo "progress-bar-info'";}
                                                elseif($percentage<=100){ echo "progress-bar-success'";}

                                                echo "role='progressbar' aria-valuenow='30' aria-valuemin='0' aria-valuemax='100' style='width:{$percentage}%'>
                                                {$percentage}%</div></div></td>";
                                                echo "</tr>";


                                        }

                                        $trimester_final_result_ = $cum_avg/$subject_count;
                                    @endphp


                            </table>
                        </div>
                    </div>

                    <div class="col-md-12 text-center" style="margin-top: 30px;">
                        <div class="col-md-4 col-md-offset-4">
                            <h2><strong>Score : {{number_format($trimester_final_result,1)}}%</strong></h2>
                        </div>
                    </div>

                </div>
                <!-- /.col -->
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->

@stop

@section('style')



@stop

@section('script')



    <!-- Page script -->
    <script>

    </script>

@stop