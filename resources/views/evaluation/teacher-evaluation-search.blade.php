@extends('layouts.fixed')

@section('title','Faculty Evaluation System')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Search Teacher Evaluation</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fas fa-tachometer-alt"></i> Home</a></li>
            <li><a href="#">Evaluation</a></li>

        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- SELECT2 EXAMPLE -->
        <div class="box box-default">
            <div class="box-header with-border">
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fas fa-times"></i></button>
                </div>
                <br>
            </div>

            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    {{ Form::open(['action' => 'EvaluationController@view','method'=>'post','id' => 'form']) }}
                    <div class="col-md-8 col-md-offset-2">
                        <div class="form-group">
                            {{ Form::label('Department:') }}
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fas fa-calendar-alt"></i>
                                </div>
                                {{ Form::select('department_id',$departments,null,['class'=>'form-control select2','id'=>'department_id','placeholder'=>'Select Department'])}}
                            </div>
                        </div>
                        <div class="form-group">
                            {{ Form::label('Teacher Name:') }}
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fas fa-calendar-alt"></i>
                                </div>
{{--                                {{ Form::select('teacher_id',$teachers,null,['class'=>'form-control select2','id'=>'teacher_id','placeholder'=>'Select Teacher'])}}--}}
                                <select class="form-control" id="teacher_id" name="teacher_id">

                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('Batch:') }}
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fas fa-calendar-alt"></i>
                                </div>
                                {{--                                {{ Form::select('batch_id',$batches,null,['class'=>'form-control select2','placeholder'=>'Select Batch'])}}--}}
                                <select class="form-control select2" id="batch_id" name="batch_id">

                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('Subject Name:') }}
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fas fa-calendar-alt"></i>
                                </div>
{{--                                {{ Form::select('subject_id',$teachers,null,['class'=>'form-control select2','id'=>'teacher_id','placeholder'=>'Select Teacher'])}}--}}
                                <select class="form-control select2" id="subject_id" name="subject_id">

                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-10">
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>
                    </div>

                    {{Form::close()}}

                </div>
                <!-- /.col -->
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->

@stop

@section('style')



@stop

@section('script')
    <script>
        $(document).ready(function () {
            $("#teacher_id").select2({
                placeholder:'Select Teacher',
                tags:true
            });

            $("#subject_id").select2({
                placeholder:'Select Subject',
                tags:true
            });

            $("#batch_id").select2({
                placeholder:'Select Batch',
                tags:true
            });
        })
    </script>
    <script>
        $('#department_id').change(function () {
            var department_id = $(this).val();
            var token = "{{csrf_token()}}";

            $.ajax({
                type:'POST',
                url:"{{route('load_teacher')}}",
                data:{department_id:department_id,_token:token},
                success:function(data) {
                    $("#teacher_id").html(data);
                }
            });
        });

        $('#teacher_id').change(function () {
            var teacher_id = $(this).val();
            var token = "{{csrf_token()}}";

            $.ajax({
                type:'POST',
                url:"{{route('load_batch')}}",
                data:{teacher_id:teacher_id,_token:token},
                success:function(data) {
                    $("#batch_id").html(data);
                }
            });
        });

        $('#batch_id').change(function () {
            var batch_id = $(this).val();
            var teacher_id = $('#teacher_id').val();
            var token = "{{csrf_token()}}";

            $.ajax({
                type:'POST',
                url:"{{route('load_subject')}}",
                data:{teacher_id:teacher_id,batch_id:batch_id,_token:token},
                success:function(data) {
                    $("#subject_id").html(data);
                }
            });
        });
    </script>
@stop