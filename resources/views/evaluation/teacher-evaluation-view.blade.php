@extends('layouts.fixed')

@section('title','Faculty Evaluation System')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Teacher Progression</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fas fa-tachometer-alt"></i> Home</a></li>
            <li><a href="#">Teacher Progression</a></li>

        </ol>
    </section>
    <!-- Main content -->
    <section class="content">

        <!-- SELECT2 EXAMPLE -->
        <div class="box box-default">
            <div class="box-header with-border">
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fas fa-times"></i></button>
                </div>
                <br>
            </div>

            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    {{--my content goes here--}}
                    <div class="col-md-11 col-md-offset-1">
                        <h1><strong>{{$teacher->name}}</strong></h1>
                    </div>
                    <div class="col-md-12">
                        <div class="progress">
                            <div class="progress-bar progress-bar-striped active
                            @php
                                if($percentage<40){ echo "progress-bar-danger";}
                                elseif($percentage<60){ echo "progress-bar-warning";}
                                elseif($percentage<80){ echo "progress-bar-info";}
                                elseif($percentage<=100){ echo "progress-bar-success";}
                            @endphp"

                                 role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width:{{$percentage}}%">
                                {{$percentage}}%
                            </div>
                        </div>

                        <div class="col-md-5 col-md-offset-2">
                            {{ Form::label('Name') }}
                            <p>{{$teacher->name}}</p>
                        </div>

                        <div class="col-md-4 col-md-offset-1">
                            {{ Form::label('Department') }}
                            <p>{{$teacher->department->name}}</p>
                        </div>

                        <div class="col-md-5 col-md-offset-2">
                            {{ Form::label('E-Mail') }}
                            <p>{{$teacher->email}}</p>
                        </div>

                        <div class="col-md-4 col-md-offset-1">
                            {{ Form::label('Joining Date') }}
                            <p>{{$teacher->join_date}}</p>
                        </div>

                        <div class="col-md-5 col-md-offset-2">
                            {{ Form::label('Subject Taught') }}
                            <p>{{$subject->name}}</p>
                        </div>

                        <div class="col-md-4 col-md-offset-1">
                            {{ Form::label('Batch Name') }}
                            <p>{{$batch->department->name}} {{$batch->name}}</p>
                        </div>
                    </div>

                    <div class="col-md-12 text-center" style="margin-top: 30px;">
                        <div class="col-md-4 col-md-offset-4">
                            <h2><strong>Score : {{$percentage}}</strong></h2>
                        </div>
                    </div>

                </div>
                <!-- /.col -->
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->

@stop

@section('style')



@stop

@section('script')



    <!-- Page script -->
    <script>

    </script>

@stop