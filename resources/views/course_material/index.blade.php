@extends('layouts.fixed')

@section('title','Faculty Evaluation System')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Department</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fas fa-tachometer-alt"></i> Home</a></li>
            <li><a href="#">Department</a></li>

        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    {{--<div class="box-header">--}}
                        {{--<button class="btn btn-primary"><a href="{{url('department-create')}}" style="color: white;"> Add Department</a></button>--}}
                    {{--</div>--}}
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="myTable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>SL NO</th>
                                    <th>Department</th>
                                    <th>Batch</th>
                                    <th>Trimester</th>
                                    <th>Subject Name</th>
                                    <th>File name</th>
                                    <th>Description</th>
                                    <th>Uploaded at</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $i = 1; ?>
                            @foreach($file_uploads as $file_upload)
                                <tr>
                                    <th> {{$i++}}</th>
                                    <td> {{$file_upload->department->name}}</td>
                                    <td> {{$file_upload->batch->name}}</td>
                                    <td> {{$file_upload->trimester->name}}</td>
                                    <td> {{$file_upload->subject->name}}</td>
                                    <td>
                                        <a href="javascript:void(0);"
                                           onclick="window.open('{{asset('uploads/').'/'.$file_upload->file_name}}');"
                                         >{{$file_upload->file_name}}</a>
                                    </td>
                                    <td> {{$file_upload->description}}</td>
                                    <td> {{$file_upload->created_at}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->

@stop

@section('script')

    <!-- DataTables -->
    <script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>

    <!-- page script -->
    <script>
        $(function () {
            $('#example1').DataTable();
            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : false,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })
        })
    </script>
@stop