@extends('layouts.fixed')
@section('title','Faculty Evaluation System')
@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> Assign Subject</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fas fa-tachometer-alt"></i> Home</a></li>
            <li><a href="#">Assign Subject</a></li>

        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- SELECT2 EXAMPLE -->
        <div class="box box-default">
            <div class="box-header with-border">
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fas fa-times"></i></button>
                </div>
                <br>
            </div>

            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    {{ Form::open(['action' => 'AssignSubjectController@create','method'=>'post','id' => 'form']) }}
                    <div class="col-md-8 col-md-offset-2">
                        <div class="form-group">
                            {{ Form::label(' Select Student ID:') }}
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fas fa-calendar-alt"></i>
                                </div>
                                {{ Form::select('student_id',$students,null,['class'=>'form-control select2','id'=>'student_id','placeholder'=>'Select Student ID'])}}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label(' Select Trimester:') }}
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fas fa-calendar-alt"></i>
                                </div>
                                {{ Form::select('trimester_id',$trimesters,null,['class'=>'form-control select2','id'=>'trimester_id','placeholder'=>'Select trimester'])}}
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-10">
                                <button type="submit" class="btn btn-success" id="student_search">Search</button>
                            </div>
                        </div>
                    </div>
                    {{Form::close()}}
                </div>
                <!-- /.col -->
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->

@stop

@section('style')



@stop
@section('script')
@stop