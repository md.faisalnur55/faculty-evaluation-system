@extends('layouts.fixed')

@section('title','Faculty Evaluation System')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Assign Subject</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fas fa-tachometer-alt"></i> Home</a></li>
            <li><a href="#">Assign Subject</a></li>

        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        {{--<button class="btn btn-primary"><a href="{{url('student-create')}}" style="color: white;"> Add Assign Subject</a></button>--}}
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            {{ Form::open(['action' => 'AssignSubjectController@store','method'=>'post','id' => 'form']) }}
                            {{Form::hidden('student_id',$student->id)}}
                            <div class="col-md-12">
                                <label>Student ID :</label>
                                <span>{{$student->card_id}}</span>
                            </div>
                            <div class="col-md-12">
                                <label>Student Name :</label>
                                <span>{{$student->name}}</span>
                            </div>
                            <div class="col-md-12">
                                <label>Father's Name :</label>
                                <span>{{$student->f_name}}</span>
                            </div>
                            <div class="col-md-12">
                                <label>Mother's Name :</label>
                                <span>{{$student->m_name}}</span>
                            </div>
                            <div class="col-md-12">
                                <label>Phone</label>
                                <span>{{$student->phone}}</span>
                            </div>
                        </div>
                        <br />

                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th class="text-center"><input type="checkbox" name="status" id="checkbox" /></th>
                                <th>Subject Name</th>
                                <th>Course Code</th>
                                <th>Credit Hours</th>
                                {{--<th>Email</th>--}}
                                {{--<th>Password</th>--}}
                                {{--<th>Action</th>--}}
                            </tr>
                            </thead>
                            <tbody>
                            @php $sl=0; @endphp
                            @foreach($subjects as $subject)
                                <tr>
                                    <td class="text-center"><input  type="checkbox" name="subject_id[]" value="{{$subject->id}}" class="checkBoxClass" />
                                        {{--<input type="hidden" name="subject_id[]" value="{{$subject->id}}" />--}}
                                    </td>
                                    <th> {{$subject->name}}</th>
                                    <td> {{$subject->course_code}}</td>
                                    <td> {{$subject->credit}}</td>
                                    {{--                                    <td> {{$student->email}}</td>--}}
                                    {{--<td> {{$student->password}}</td>--}}
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-10 text-center">
                            <button type="submit" class="btn btn-success" id="student_search">Assign Subjects</button>
                        </div>
                    </div>
                    {{Form::close()}}
                    <br>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->

@stop

@section('script')

    <!-- DataTables -->
    <script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>

    <!-- page script -->
    <script>
        $(function () {
            $('#example1').DataTable();
            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : false,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })
        })
    </script>
    <script>
        var as=0;
        $("#checkbox").click(function () {
            $(".checkBoxClass").prop('checked', $(this).prop('checked'));
        });
    </script>
@stop