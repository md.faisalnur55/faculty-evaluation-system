<!-- sidebar: style can be found in sidebar.less -->
<section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
        <div class="pull-left image">
            <img src="{{ asset('dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
            <p>{{ Auth::user()->name}}</p>
            <a href="#"><i class="fas fa-circle text-success"></i> Online</a>
        </div>
    </div>
    <!-- search form -->
    <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
            <input type="text" name="q" class="form-control" placeholder="Search...">
            <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                  <i class="fas fa-search"></i>
                </button>
              </span>
        </div>
    </form>
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
            @php
                $permission_keys=[];
                foreach (\Illuminate\Support\Facades\Auth::user()->roles as $role){
                    foreach ($role->permissions as $permission){
                        array_push($permission_keys,url('/').'/'.$permission->permission_key);
                    }
                }
            @endphp

    <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>

        <li class="{{ isActive(['/*']) }}">
            <a href="{{ url('/') }}">
                <i class="fas fa-tachometer-alt"></i> <span>Dashboard</span>

            </a>
        </li>
        {{--{{dd($permission_keys)}}--}}
        @if(in_array(route('role.create'),$permission_keys))
        <li class="treeview {{ isActive('role*') }}">
            <a href="#"1>
                <i class="fas fa-edit"></i> <span>Role Management</span>
                <span class="pull-right-container">
              <i class="fas fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
                <li class="{{ isActive('role-create') }}"><a href="{{ url('role-create') }}">Create role</a></li>
                {{--<li class="{{ isActive('role-manage') }}"><a href="{{ url('role-manage') }}"> Manage Role</a></li>--}}
            </ul>
        </li>
        @endif
        @if(in_array(route('user.create'),$permission_keys) or in_array(route('user.manage'),$permission_keys))
        <li class="treeview {{ isActive('user*') }}">
            <a href="#"1>
                <i class="fas fa-edit"></i> <span>User Management</span>
                <span class="pull-right-container">
              <i class="fas fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
                <li class="{{ isActive('user-create') }}"><a href="{{ url('user-create') }}">Create User</a></li>
                <li class="{{ isActive('user-manage') }}"><a href="{{ url('user-manage') }}"> Manage User</a></li>
            </ul>
        </li>
        @endif
        {{--<li class="treeview {{ isActive('permission*') }}">--}}
            {{--<a href="#"1>--}}
                {{--<i class="fas fa-edit"></i> <span>Permission Management</span>--}}
                {{--<span class="pull-right-container">--}}
              {{--<i class="fas fa-angle-left pull-right"></i>--}}
            {{--</span>--}}
            {{--</a>--}}
            {{--<ul class="treeview-menu">--}}
                {{--<li class="{{ isActive('permission-create') }}"><a href="{{ url('permission-create') }}">Create Permission</a></li>--}}
                {{--<li class="{{ isActive('permission-manage') }}"><a href="{{ url('permission-manage') }}">Permission Manage</a></li>--}}
            {{--</ul>--}}
        {{--</li>--}}

        {{--Teacher Start--}}
        @if(in_array(route('teacher.manage'),$permission_keys) or in_array(route('teacher.create'),$permission_keys))
        <li class="treeview {{ isActive(['teacher*']) }}">
            <a href="#"1>
                <i class="fas fa-edit"></i> <span>Teacher</span>
                <span class="pull-right-container">
              <i class="fas fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
                <li class="{{ isActive('teacher-manage') }}"><a href="{{ url('teacher-manage') }}">All Teacher</a></li>
                <li class="{{ isActive('teacher-create') }}"><a href="{{ url('teacher-create') }}">Add Teacher</a></li>
            </ul>
        </li>
        @endif
        {{--Teacher End--}}

        {{--Student Start--}}
        @if(in_array(route('student.create'),$permission_keys) or in_array(route('student.manage'),$permission_keys))
        <li class="treeview {{ isActive(['student*']) }}">
            <a href="#">
                <i class="fas fa-edit"></i> <span>Student</span>
                <span class="pull-right-container">
              <i class="fas fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
                <li class="{{ isActive('student-manage') }}"><a href="{{ url('student-manage') }}">All Student</a></li>
                <li class="{{ isActive('student-create') }}"><a href="{{ url('student-create') }}">Add Student</a></li>
            </ul>
        </li>
        @endif
        {{--Student End--}}

        {{--Assign Subject Start--}}
        @if(in_array(route('assign_subject.create'),$permission_keys)
        or in_array(route('assign_subject.manage'),$permission_keys)
        or in_array(route('assign_subject_teacher.create'),$permission_keys)
        or in_array(route('assign_subject_teacher.manage'),$permission_keys)
        )
        <li class="treeview {{ isActive(['assign*']) }}">
            <a href="#">
                <i class="fas fa-edit"></i> <span>Subject Assignation</span>
                <span class="pull-right-container">
              <i class="fas fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
                {{--@if(in_array(route('assign_subject.manage'),$permission_keys))--}}
                    {{--<li class="{{ isActive('assign-subject-manage') }}"><a href="{{ url('assign-subject-manage') }}">View Assigned Subjects to Students</a></li>--}}
                {{--@endif--}}
                @if(in_array(route('assign_subject_teacher.manage'),$permission_keys))
                    <li class="{{ isActive('assign-subject-teacher-view') }}"><a href="{{ url('assign-subject-teacher-view') }}">View Assigned Subjects to Teachers</a></li>
                @endif
                @if(in_array(route('assign_subject.create'),$permission_keys))
                        <li class="{{ isActive('assign-subject-manage') }}"><a href="{{ url('assign-subject-manage') }}">Assign Subject to Student</a></li>
                @endif
                @if(in_array(route('assign_subject_teacher.create'),$permission_keys))
                        <li class="{{ isActive('assign-subject-teacher-manage') }}"><a href="{{ url('assign-subject-teacher-manage') }}">Assign Subject to Teacher</a></li>
                @endif
            </ul>
        </li>
        @endif
        {{--Teacher End--}}

        {{--File Upload Start--}}
        @if(in_array(route('file_upload.create'),$permission_keys) or in_array(route('file_upload.manage'),$permission_keys))

        <li class="treeview {{ isActive(['file-upload*']) }}">
            <a href="#">
                <i class="fas fa-edit"></i> <span>File Upload</span>
                <span class="pull-right-container">
              <i class="fas fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
                @if(in_array(route('file_upload.create'),$permission_keys))
                    <li class="{{ isActive('file-upload-manage') }}"><a href="{{ url('file-upload-manage') }}">View Uploaded File</a></li>
                @endif
                @if(in_array(route('file_upload.create'),$permission_keys))
                    <li class="{{ isActive('file-upload-create') }}"><a href="{{ url('file-upload-create') }}">Upload New File</a></li>
                @endif
            </ul>
        </li>
        @endif
        {{--File Upload End--}}

        {{--Course Material Start--}}
        @if(in_array(route('course.search'),$permission_keys) or in_array(route('course.manage'),$permission_keys))
        <li class="treeview {{ isActive(['course-material*']) }}">
            <a href="#">
                <i class="fas fa-edit"></i> <span>Course Materials</span>
                <span class="pull-right-container">
              <i class="fas fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
                @if(in_array(route('course.search'),$permission_keys))
                    <li class="{{ isActive('course-material-search') }}"><a href="{{ url('course-material-search') }}">View Course Materials</a></li>
                @endif
            </ul>
        </li>
        @endif
        {{--Course Material End--}}

        {{--Settings Start--}}

        @if(in_array(route('department.manage'),$permission_keys)
        or in_array(route('batch.manage'),$permission_keys)
        or in_array(route('trimester.manage'),$permission_keys)
        or in_array(route('subject.manage'),$permission_keys)
        or in_array(route('question.manage'),$permission_keys)
        )

        <li class="treeview {{ isActive(['settings*','department*','trimester*','subject*','question*','batch*']) }}">
            <a href="#"1>
                <i class="fas fa-edit"></i> <span>Settings</span>
                <span class="pull-right-container">
              <i class="fas fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
                @if(in_array(route('department.manage'),$permission_keys))
                    <li class="{{ isActive('department-manage') }}"><a href="{{ url('department-manage') }}">Department Setting</a></li>
                @endif
                @if(in_array(route('batch.manage'),$permission_keys))
                    <li class="{{ isActive('batch-manage') }}"><a href="{{ url('batch-manage') }}">Batch Setting</a></li>
                @endif
                @if(in_array(route('trimester.manage'),$permission_keys))
                    <li class="{{ isActive('trimester-manage') }}"><a href="{{ url('trimester-manage') }}">Trimester Setting</a></li>
                @endif
                @if(in_array(route('subject.manage'),$permission_keys))
                    <li class="{{ isActive('subject-manage') }}"><a href="{{ url('subject-manage') }}">Subject Setting</a></li>
                @endif
                @if(in_array(route('question.manage'),$permission_keys))
                    <li class="{{ isActive('question-manage') }}"><a href="{{ url('question-manage') }}">Question Setting</a></li>
                @endif
            </ul>
        </li>

        @endif
        {{--Settings End--}}

        {{--Evaluate Teacher Start--}}

        @if(in_array(route('evaluation.search'),$permission_keys) or in_array(route('evaluation.manage'),$permission_keys))

        <li class="treeview {{ isActive(['evaluation*']) }}">
            <a href="#"1>
                <i class="fas fa-edit"></i> <span>Teacher Evaluation</span>
                <span class="pull-right-container">
              <i class="fas fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
                @if(in_array(route('evaluation.manage'),$permission_keys))
                    <li class="{{ isActive('evaluation-manage') }}"><a href="{{ url('evaluation-manage') }}">Evaluate Teacher</a></li>
                @endif
                @if(in_array(route('evaluation.search'),$permission_keys))
                    <li class="{{ isActive('evaluation-search') }}"><a href="{{ url('evaluation-search') }}">View Subject Wise Percentage</a></li>
                @endif
                @if(in_array(route('evaluation.search.total'),$permission_keys))
                    <li class="{{ isActive('evaluation-total-search') }}"><a href="{{ url('evaluation-total-search') }}">View Total Percentage</a></li>
                @endif
            </ul>
        </li>
        @endif
        {{--Evaluate Teacher End--}}

        {{--Faculty Evaluation Result Send Start--}}
        @if(in_array(route('send_email.create'),$permission_keys))
        <li class="treeview {{ isActive(['send-faculty-evaluation-result-search*']) }}">
            <a href="#"1>
                <i class="fas fa-edit"></i> <span>Evaluation Result Mailing</span>
                <span class="pull-right-container">
              <i class="fas fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
                @if(in_array(route('send_email.create'),$permission_keys))
                    <li class="{{ isActive('send-faculty-evaluation-result-search') }}"><a href="{{ url('send-faculty-evaluation-result-search') }}">Send Evaluation Result</a></li>
                @endif
            </ul>
        </li>
        @endif
        {{--Evaluate Teacher End--}}
    </ul>
</section>
<!-- /.sidebar -->