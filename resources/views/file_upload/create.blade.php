@extends('layouts.fixed')

@section('title','Faculty Evaluation System')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Upload File</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fas fa-tachometer-alt"></i> Home</a></li>
            <li><a href="#">Upload File</a></li>

        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- SELECT2 EXAMPLE -->
        <div class="box box-default">
            <div class="box-header with-border">
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fas fa-times"></i></button>
                </div>
                <br>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    {{ Form::open(['action' => 'FileUploadController@store','method'=>'post','id' => 'form','files'=>true]) }}
                    <div class="col-md-8 col-md-offset-2">
                        <div class="form-group">
                            {{ Form::label('Department:') }}
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fas fa-calendar-alt"></i>
                                </div>
                                {{ Form::select('department_id',$departments,null,['class'=>'form-control select2','placeholder'=>'Select Department','id'=>'department_id'])}}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('Batch:') }}
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fas fa-calendar-alt"></i>
                                </div>
                                <select id="batch_id" name="batch_id" class="form-control select2">

                                </select>
{{--                                {{ Form::select('batch_id',$departments,null,['class'=>'form-control select2','placeholder'=>'Select Batch'])}}--}}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('Trimester:') }}
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fas fa-calendar-alt"></i>
                                </div>
                                {{ Form::select('trimester_id',$trimesters,null,['class'=>'form-control select2','placeholder'=>'Select Trimester','id'=>'trimester_id'])}}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('Subject :') }}
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fas fa-calendar-alt"></i>
                                </div>
                                {{--{{ Form::select('subject_id',$departments,null,['class'=>'form-control select2','placeholder'=>'Select Subject'])}}--}}
                                <select id="subject_id" name="subject_id" class="form-control select2">

                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            {{ Form::label('Upload File:') }}
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fas fa-calendar-alt"></i>
                                </div>
                                {{ Form::file('file_name',['class'=>'form-control']) }}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('Description:') }}
                            <div class="input-group">
                                <div class="input-group-addon">
                                    {{--<i class="fas fa-calendar-alt"></i>--}}
                                </div>
                                {{ Form::textarea('description',null,['class'=>'form-control']) }}
                            </div>
                        </div>



                        <div class="form-group row">
                            <div class="col-md-10">
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>
                    </div>

                    {{Form::close()}}

                </div>
                <!-- /.col -->
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->

@stop

@section('style')



@stop

@section('script')



    <!-- Page script -->
    <script>
        $('#department_id').change(function () {
            var department_id = $('#department_id').val();
            var csrf = "{{csrf_token()}}";
            $.ajax({
                type:"post",
                url:"{{route('load_batches')}}",
                data: {department_id:department_id,_token:csrf},
                success:function(data) {
                    $('#batch_id').html(data);
                }
            })
        });
    </script>
    <script>
        $('#batch_id').change(function () {
            var batch_id = $(this).val();
            var csrf = "{{csrf_token()}}";
            $.ajax({
                type:"post",
                url:"{{route('load_subjects')}}",
                data: {batch_id:batch_id,_token:csrf},
                success:function(data) {
                    $('#subject_id').html(data);
                }
            })
        });
    </script>
@stop