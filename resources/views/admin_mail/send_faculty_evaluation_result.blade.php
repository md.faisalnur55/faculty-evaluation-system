<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>Hello {{$teacher->name}}, Your {{$trimester_all->name}} evaluation is below ...</h1>
    <table border="1" cellspacing="0" cellpadding="5">
        <tr>
            <th>Subject Name</th>
            <th>Batch</th>
            <th>Percentage</th>
        </tr>
        <tr>
            <td>{{$subject_all->name}}</td>
            <td>{{$batch->name}}</td>
            <td>{{$percentage}}</td>
        </tr>
    </table>

</body>
</html>

