@extends('layouts.fixed')

@section('title','Faculty Evaluation System')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Create Role</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fas fa-tachometer-alt"></i> Home</a></li>
            <li><a href="#">Edit Role</a></li>

        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- SELECT2 EXAMPLE -->
        <div class="box box-default">
            <div class="box-header with-border">
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fas fa-times"></i></button>
                </div>
                <br>
            </div>

            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Edit Role</h3>
                            </div>

                            <div class="panel-body">
                                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                    <div class="panel-body">
                                        <div class="row">
                                            {{--vaccine-transfer Start--}}
                                            <div class="col-lg-5 col-sm-5 col-md-6 col-xs-12">
                                                {{ Form::model($role,['route'=>['role.update',$role->id],'method'=>'post']) }}
                                                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 {{$errors->has('name') ? 'has-error' : ''}}">
                                                    <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                                    {{ Form::label('name','Role Name:* ',['class'=>'control-label','required'])}}
                                                    {{ Form::text('name',null,['class'=>'form-control','id'=>'name'])}}
                                                    @if($errors->has('name'))
                                                        <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
                                                    @endif
                                                </div>
                                                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 {{$errors->has('permission') ? 'has-error' : ''}}">
                                                    {{--<span style="display: block;height: 10px;width: 100%;background: #fff;"></span>--}}
                                                    {{--                                            {{ Form::label('permission','Permission :* ',['class'=>'control-label'])}}--}}

                                                    <div class="col-md-12"><br></div>
                                                    <label for="permission"><h4><strong>Permissions</strong></h4></label>
                                                    <div class="col-md-12"><br></div>

                                                    {{--User Permission Start--}}
                                                    <div class="col-md-4">
                                                        <h5>User</h5>
                                                    </div>
                                                    <div class="col-md-4 checkbox">
                                                        <input id="user-select-all" type="checkbox">
                                                        <label for="demo-form-checkbox"> Select all</label>
                                                    </div>

                                                    <div class="col-md-4">
                                                        {{--@foreach($permissions as $permission)--}}
                                                        @foreach($user as $user_permission)
                                                            @php
                                                                foreach($currentpermissions as $currentpermission){
                                                                    if($currentpermission->permission_key == $user_permission->permission_key){$check = 'checked';break;}else{$check = '';};
                                                                }
                                                            @endphp
                                                            <div class="checkbox">
                                                                <input  type="checkbox" @php if($check == 'checked'){echo $check;}else{}  @endphp  class="user-select" name="asignpermission[]" value="{{$user_permission->id}}" />
                                                                <label for="demo-form-checkbox">{{$user_permission->name}}</label>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                    <div class="col-md-12"><hr /></div>
                                                    {{--User Permission End--}}
                                                    {{--Role Permission Start--}}
                                                    <div class="col-md-4">
                                                        <h5>Role</h5>
                                                    </div>
                                                    <div class="col-md-4 checkbox">
                                                        <input id="role-select-all" type="checkbox">
                                                        <label for="demo-form-checkbox"> Select all</label>
                                                    </div>

                                                    <div class="col-md-4">
                                                        {{--@foreach($permissions as $permission)--}}
                                                        {{--{{dd($role_permission)}}--}}
                                                        @foreach($role_permission as $user_permission)
                                                            @php
                                                                foreach($currentpermissions as $currentpermission){
                                                                    if($currentpermission->permission_key == $user_permission->permission_key){$check = 'checked';break;}else{$check = '';};
                                                                }
                                                            @endphp
                                                            <div class="checkbox">
                                                                <input  type="checkbox" @php if($check == 'checked'){echo $check;}else{}  @endphp  class="role-select" name="asignpermission[]" value="{{$user_permission->id}}" />
                                                                <label for="demo-form-checkbox">{{$user_permission->name}}</label>
                                                            </div>
                                                        @endforeach
                                                    </div>

                                                    <div class="col-md-12"><hr /></div>
                                                    {{--Role Permission End--}}

                                                    {{--Teacher Permission Start--}}
                                                    <div class="col-md-4">
                                                        <h5>Teacher</h5>
                                                    </div>
                                                    <div class="col-md-4 checkbox">
                                                        <input id="teacher-select-all" type="checkbox">
                                                        <label for="demo-form-checkbox"> Select all</label>
                                                    </div>

                                                    <div class="col-md-4">
                                                        {{--@foreach($permissions as $permission)--}}
                                                        {{--{{dd($role_permission)}}--}}
                                                        @foreach($teacher as $user_permission)
                                                            @php
                                                                foreach($currentpermissions as $currentpermission){
                                                                    if($currentpermission->permission_key == $user_permission->permission_key){$check = 'checked';break;}else{$check = '';};
                                                                }
                                                            @endphp
                                                            <div class="checkbox">
                                                                <input  type="checkbox" @php if($check == 'checked'){echo $check;}else{}  @endphp  class="teacher-select" name="asignpermission[]" value="{{$user_permission->id}}" />
                                                                <label for="demo-form-checkbox">{{$user_permission->name}}</label>
                                                            </div>
                                                        @endforeach
                                                    </div>

                                                    <div class="col-md-12"><hr /></div>
                                                    {{--Teacher Permission End--}}

                                                    {{--Student Permission Start--}}
                                                    <div class="col-md-4">
                                                        <h5>Student</h5>
                                                    </div>
                                                    <div class="col-md-4 checkbox">
                                                        <input id="student-select-all" type="checkbox">
                                                        <label for="demo-form-checkbox"> Select all</label>
                                                    </div>

                                                    <div class="col-md-4">
                                                        {{--@foreach($permissions as $permission)--}}
                                                        {{--{{dd($role_permission)}}--}}
                                                        @foreach($student as $user_permission)
                                                            @php
                                                                foreach($currentpermissions as $currentpermission){
                                                                    if($currentpermission->permission_key == $user_permission->permission_key){$check = 'checked';break;}else{$check = '';};
                                                                }
                                                            @endphp
                                                            <div class="checkbox">
                                                                <input  type="checkbox" @php if($check == 'checked'){echo $check;}else{}  @endphp  class="student-select" name="asignpermission[]" value="{{$user_permission->id}}" />
                                                                <label for="demo-form-checkbox">{{$user_permission->name}}</label>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                    <div class="col-md-12"><hr /></div>
                                                    {{--Student Permission End--}}
                                                    {{--Department Permission Start--}}
                                                    <div class="col-md-4">
                                                        <h5>Department</h5>
                                                    </div>
                                                    <div class="col-md-4 checkbox">
                                                        <input id="department-select-all" type="checkbox">
                                                        <label for="demo-form-checkbox"> Select all</label>
                                                    </div>

                                                    <div class="col-md-4">
                                                        {{--@foreach($permissions as $permission)--}}
                                                        {{--{{dd($role_permission)}}--}}
                                                        @foreach($department as $user_permission)
                                                            @php
                                                                foreach($currentpermissions as $currentpermission){
                                                                    if($currentpermission->permission_key == $user_permission->permission_key){$check = 'checked';break;}else{$check = '';};
                                                                }
                                                            @endphp
                                                            <div class="checkbox">
                                                                <input  type="checkbox" @php if($check == 'checked'){echo $check;}else{}  @endphp  class="department-select" name="asignpermission[]" value="{{$user_permission->id}}" />
                                                                <label for="demo-form-checkbox">{{$user_permission->name}}</label>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                    <div class="col-md-12"><hr /></div>
                                                    {{--Department Permission End--}}
                                                    {{--Batch Permission Start--}}
                                                    <div class="col-md-4">
                                                        <h5>Batch</h5>
                                                    </div>
                                                    <div class="col-md-4 checkbox">
                                                        <input id="batch-select-all" type="checkbox">
                                                        <label for="demo-form-checkbox"> Select all</label>
                                                    </div>

                                                    <div class="col-md-4">
                                                        {{--@foreach($permissions as $permission)--}}
                                                        {{--{{dd($role_permission)}}--}}
                                                        @foreach($batch as $user_permission)
                                                            @php
                                                                foreach($currentpermissions as $currentpermission){
                                                                    if($currentpermission->permission_key == $user_permission->permission_key){$check = 'checked';break;}else{$check = '';};
                                                                }
                                                            @endphp
                                                            <div class="checkbox">
                                                                <input  type="checkbox" @php if($check == 'checked'){echo $check;}else{}  @endphp  class="batch-select" name="asignpermission[]" value="{{$user_permission->id}}" />
                                                                <label for="demo-form-checkbox">{{$user_permission->name}}</label>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                    <div class="col-md-12"><hr /></div>
                                                    {{--Batch Permission End--}}


                                                    {{--Trimester Permission Start--}}
                                                    <div class="col-md-4">
                                                        <h5>Trimester</h5>
                                                    </div>
                                                    <div class="col-md-4 checkbox">
                                                        <input id="trimester-select-all" type="checkbox">
                                                        <label for="demo-form-checkbox"> Select all</label>
                                                    </div>

                                                    <div class="col-md-4">
                                                        {{--@foreach($permissions as $permission)--}}
                                                        {{--{{dd($role_permission)}}--}}
                                                        @foreach($trimester as $user_permission)
                                                            @php
                                                                foreach($currentpermissions as $currentpermission){
                                                                    if($currentpermission->permission_key == $user_permission->permission_key){$check = 'checked';break;}else{$check = '';};
                                                                }
                                                            @endphp
                                                            <div class="checkbox">
                                                                <input  type="checkbox" @php if($check == 'checked'){echo $check;}else{}  @endphp  class="trimester-select" name="asignpermission[]" value="{{$user_permission->id}}" />
                                                                <label for="demo-form-checkbox">{{$user_permission->name}}</label>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                    <div class="col-md-12"><hr /></div>
                                                    {{--User Permission End--}}


                                                    {{--Subject Permission Start--}}
                                                    <div class="col-md-4">
                                                        <h5>Subject</h5>
                                                    </div>
                                                    <div class="col-md-4 checkbox">
                                                        <input id="subject-select-all" type="checkbox">
                                                        <label for="demo-form-checkbox"> Select all</label>
                                                    </div>

                                                    <div class="col-md-4">
                                                        {{--@foreach($permissions as $permission)--}}
                                                        {{--{{dd($role_permission)}}--}}
                                                        @foreach($subject as $user_permission)
                                                            @php
                                                                foreach($currentpermissions as $currentpermission){
                                                                    if($currentpermission->permission_key == $user_permission->permission_key){$check = 'checked';break;}else{$check = '';};
                                                                }
                                                            @endphp
                                                            <div class="checkbox">
                                                                <input  type="checkbox" @php if($check == 'checked'){echo $check;}else{}  @endphp  class="subject-select" name="asignpermission[]" value="{{$user_permission->id}}" />
                                                                <label for="demo-form-checkbox">{{$user_permission->name}}</label>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                    <div class="col-md-12"><hr /></div>
                                                    {{--User Permission End--}}


                                                    {{--Question Permission Start--}}
                                                    <div class="col-md-4">
                                                        <h5>Question</h5>
                                                    </div>
                                                    <div class="col-md-4 checkbox">
                                                        <input id="question-select-all" type="checkbox">
                                                        <label for="demo-form-checkbox"> Select all</label>
                                                    </div>

                                                    <div class="col-md-4">
                                                        {{--@foreach($permissions as $permission)--}}
                                                        {{--{{dd($role_permission)}}--}}
                                                        @foreach($question as $user_permission)
                                                            @php
                                                                foreach($currentpermissions as $currentpermission){
                                                                    if($currentpermission->permission_key == $user_permission->permission_key){$check = 'checked';break;}else{$check = '';};
                                                                }
                                                            @endphp
                                                            <div class="checkbox">
                                                                <input  type="checkbox" @php if($check == 'checked'){echo $check;}else{}  @endphp  class="question-select" name="asignpermission[]" value="{{$user_permission->id}}" />
                                                                <label for="demo-form-checkbox">{{$user_permission->name}}</label>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                    <div class="col-md-12"><hr /></div>
                                                    {{--Question Permission End--}}

                                                    {{--'assign_subject','assign_subject_teacher','evaluation','file_upload','course_material'--}}

                                                    {{--User Permission Start--}}
                                                    <div class="col-md-4">
                                                        <h5>Assign Subject to Student</h5>
                                                    </div>
                                                    <div class="col-md-4 checkbox">
                                                        <input id="assign-subject-select-all" type="checkbox">
                                                        <label for="demo-form-checkbox"> Select all</label>
                                                    </div>

                                                    <div class="col-md-4">
                                                        {{--@foreach($permissions as $permission)--}}
                                                        {{--{{dd($role_permission)}}--}}
                                                        @foreach($assign_subject as $user_permission)
                                                            @php
                                                                foreach($currentpermissions as $currentpermission){
                                                                    if($currentpermission->permission_key == $user_permission->permission_key){$check = 'checked';break;}else{$check = '';};
                                                                }
                                                            @endphp
                                                            <div class="checkbox">
                                                                <input  type="checkbox" @php if($check == 'checked'){echo $check;}else{}  @endphp  class="assign-subject-select" name="asignpermission[]" value="{{$user_permission->id}}" />
                                                                <label for="demo-form-checkbox">{{$user_permission->name}}</label>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                    <div class="col-md-12"><hr /></div>
                                                    {{--User Permission End--}}


                                                    {{--User Permission Start--}}
                                                    <div class="col-md-4">
                                                        <h5>Assign Subject to Teacher</h5>
                                                    </div>
                                                    <div class="col-md-4 checkbox">
                                                        <input id="assign-subject-teacher-select-all" type="checkbox">
                                                        <label for="demo-form-checkbox"> Select all</label>
                                                    </div>

                                                    <div class="col-md-4">
                                                        {{--@foreach($permissions as $permission)--}}
                                                        {{--{{dd($role_permission)}}--}}
                                                        @foreach($assign_subject_teacher as $user_permission)
                                                            @php
                                                                foreach($currentpermissions as $currentpermission){
                                                                    if($currentpermission->permission_key == $user_permission->permission_key){$check = 'checked';break;}else{$check = '';};
                                                                }
                                                            @endphp
                                                            <div class="checkbox">
                                                                <input  type="checkbox" @php if($check == 'checked'){echo $check;}else{}  @endphp  class="assign-subject-teacher-select" name="asignpermission[]" value="{{$user_permission->id}}" />
                                                                <label for="demo-form-checkbox">{{$user_permission->name}}</label>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                    <div class="col-md-12"><hr /></div>
                                                    {{--User Permission End--}}


                                                    {{--Evaluation Permission Start--}}
                                                    <div class="col-md-4">
                                                        <h5>Evaluation</h5>
                                                    </div>
                                                    <div class="col-md-4 checkbox">
                                                        <input id="evaluation-select-all" type="checkbox">
                                                        <label for="demo-form-checkbox"> Select all</label>
                                                    </div>

                                                    <div class="col-md-4">
                                                        {{--@foreach($permissions as $permission)--}}
                                                        {{--{{dd($role_permission)}}--}}
                                                        @foreach($evaluation as $user_permission)
                                                            @php
                                                                foreach($currentpermissions as $currentpermission){
                                                                    if($currentpermission->permission_key == $user_permission->permission_key){$check = 'checked';break;}else{$check = '';};
                                                                }
                                                            @endphp
                                                            <div class="checkbox">
                                                                <input  type="checkbox" @php if($check == 'checked'){echo $check;}else{}  @endphp  class="evaluation-select" name="asignpermission[]" value="{{$user_permission->id}}" />
                                                                <label for="demo-form-checkbox">{{$user_permission->name}}</label>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                    <div class="col-md-12"><hr /></div>
                                                    {{--Evaluation Permission End--}}


                                                    {{--User Permission Start--}}
                                                    <div class="col-md-4">
                                                        <h5>File Upload</h5>
                                                    </div>
                                                    <div class="col-md-4 checkbox">
                                                        <input id="file-upload-select-all" type="checkbox">
                                                        <label for="demo-form-checkbox"> Select all</label>
                                                    </div>

                                                    <div class="col-md-4">
                                                        {{--@foreach($permissions as $permission)--}}
                                                        {{--{{dd($role_permission)}}--}}
                                                        @foreach($file_upload as $user_permission)
                                                            @php
                                                                foreach($currentpermissions as $currentpermission){
                                                                    if($currentpermission->permission_key == $user_permission->permission_key){$check = 'checked';break;}else{$check = '';};
                                                                }
                                                            @endphp
                                                            <div class="checkbox">
                                                                <input  type="checkbox" @php if($check == 'checked'){echo $check;}else{}  @endphp  class="file-upload-select" name="asignpermission[]" value="{{$user_permission->id}}" />
                                                                <label for="demo-form-checkbox">{{$user_permission->name}}</label>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                    <div class="col-md-12"><hr /></div>
                                                    {{--User Permission End--}}


                                                    {{--User Permission Start--}}
                                                    <div class="col-md-4">
                                                        <h5>Course Material</h5>
                                                    </div>
                                                    <div class="col-md-4 checkbox">
                                                        <input id="course-material-select-all" type="checkbox">
                                                        <label for="demo-form-checkbox"> Select all</label>
                                                    </div>

                                                    <div class="col-md-4">
                                                        {{--@foreach($permissions as $permission)--}}
                                                        {{--{{dd($role_permission)}}--}}
                                                        @foreach($course_material as $user_permission)
                                                            @php
                                                                foreach($currentpermissions as $currentpermission){
                                                                    if($currentpermission->permission_key == $user_permission->permission_key){$check = 'checked';break;}else{$check = '';};
                                                                }
                                                            @endphp
                                                            <div class="checkbox">
                                                                <input  type="checkbox" @php if($check == 'checked'){echo $check;}else{}  @endphp  class="course-material-select" name="asignpermission[]" value="{{$user_permission->id}}" />
                                                                <label for="demo-form-checkbox">{{$user_permission->name}}</label>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                    <div class="col-md-12"><hr /></div>
                                                    {{--User Permission End--}}
                                                    {{--User Permission Start--}}
                                                    <div class="col-md-4">
                                                        <h5>Send Faculty Result</h5>
                                                    </div>
                                                    <div class="col-md-4 checkbox">
                                                        <input id="send-select-all" type="checkbox">
                                                        <label for="demo-form-checkbox"> Select all</label>
                                                    </div>

                                                    <div class="col-md-4">
                                                        {{--@foreach($permissions as $permission)--}}
                                                        {{--{{dd($role_permission)}}--}}
                                                        @foreach($send_result as $user_permission)
                                                            @php
                                                                foreach($currentpermissions as $currentpermission){
                                                                    if($currentpermission->permission_key == $user_permission->permission_key){$check = 'checked';break;}else{$check = '';};
                                                                }
                                                            @endphp
                                                            <div class="checkbox">
                                                                <input  type="checkbox" @php if($check == 'checked'){echo $check;}else{}  @endphp  class="send-select" name="asignpermission[]" value="{{$user_permission->id}}" />
                                                                <label for="demo-form-checkbox">{{$user_permission->name}}</label>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                    <div class="col-md-12"><hr /></div>
                                                    {{--User Permission End--}}

                                                </div>

                                                <div class="col-lg-12 col-sm-12 col-xs-12"><span style="display: block;height: 10px;width: 100%;background: #fff;"></span></div>
                                                <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                                                    <div class="col-lg-4 col-sm-4 col-xs-12">
                                                        {{ Form::submit('Update',['class'=>'btn btn-success']) }}
                                                    </div>
                                                </div>
                                                {{ Form::close() }}
                                            </div>
                                            <div class="col-lg-offset-1 col-lg-6 col-sm-6 col-md-6 col-xs-12">
                                                <table id="protable" class="table table-bordered table-striped">
                                                    <thead>
                                                    <tr>
                                                        <th>SL</th>
                                                        <th>Name</th>
                                                        <th>Permissions</th>
                                                        <th>Action</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="sale_report">
                                                    @php $i =0;
                                                    @endphp
                                                    @foreach($roles as $role)
                                                        <tr>
                                                            <td>{{++$i}}</td>
                                                            <td>{{$role->name}}</td>
                                                            <td>
                                                                {{--{{$role->permissions}}--}}
                                                                <ul>
                                                                    @foreach($role->permissions as $permission)
                                                                        <li>{{$permission->name}}</li>
                                                                    @endforeach
                                                                </ul>
                                                            </td>
                                                            <td>
                                                                <a href="{{route('role.edit',$role->id)}}" class="btn btn-success fa fa-edit"></a> || <button class="btn btn-sm btn-danger fa fa-trash erase" data-id="{{$role->id}}" data-url="{{url('role/erase')}}"></button>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                            {{--vaccine-transfer End--}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>{{-- left col -lg-6 close here --}}


        </div>{{-- Panel Body End--}}
        <!-- /.box-body -->
        <!-- /.box -->
    </section>
    <!-- /.content -->

@stop

@section('style')

    <style>
        input[type='checkbox']{
            width: 50px;
            height: 20px;
        }

        input[type='checkbox']:checked + label{
            color: green;
            font-weight: bold;
        }

        input[type='checkbox'] + label {
            color: #0a6aa1;
            font-weight: 600;
        }

        label{
            line-height: 26px;
        }
        h5{
            font-weight: bold;
            font-size: large;
            color:#0a6aa1;
        }
    </style>

@stop

@section('script')



    <!-- Page script -->
    <!-- Page script -->
    <script>
        $(document).ready(function () {
            $("#user-select-all").click(function () {
                $(".user-select").prop('checked', $(this).prop('checked'));
            });
        });

        $(document).ready(function () {
            $("#role-select-all").click(function () {
                $(".role-select").prop('checked', $(this).prop('checked'));
            });
        });

        $(document).ready(function () {
            $("#teacher-select-all").click(function () {
                $(".teacher-select").prop('checked', $(this).prop('checked'));
            });
        });

        $(document).ready(function () {
            $("#student-select-all").click(function () {
                $(".student-select").prop('checked', $(this).prop('checked'));
            });
        });

        $(document).ready(function () {
            $("#department-select-all").click(function () {
                $(".department-select").prop('checked', $(this).prop('checked'));
            });
        });

        $(document).ready(function () {
            $("#batch-select-all").click(function () {
                $(".batch-select").prop('checked', $(this).prop('checked'));
            });
        });

        $(document).ready(function () {
            $("#trimester-select-all").click(function () {
                $(".trimester-select").prop('checked', $(this).prop('checked'));
            });
        });

        $(document).ready(function () {
            $("#subject-select-all").click(function () {
                $(".subject-select").prop('checked', $(this).prop('checked'));
            });
        });

        $(document).ready(function () {
            $("#question-select-all").click(function () {
                $(".question-select").prop('checked', $(this).prop('checked'));
            });
        });

        $(document).ready(function () {
            $("#assign-subject-select-all").click(function () {
                $(".assign-subject-select").prop('checked', $(this).prop('checked'));
            });
        });

        $(document).ready(function () {
            $("#assign-subject-teacher-select-all").click(function () {
                $(".assign-subject-teacher-select").prop('checked', $(this).prop('checked'));
            });
        });

        $(document).ready(function () {
            $("#evaluation-select-all").click(function () {
                $(".evaluation-select").prop('checked', $(this).prop('checked'));
            });
        });

        $(document).ready(function () {
            $("#file-upload-select-all").click(function () {
                $(".file-upload-select").prop('checked', $(this).prop('checked'));
            });
        });

        $(document).ready(function () {
            $("#course-material-select-all").click(function () {
                $(".course-material-select").prop('checked', $(this).prop('checked'));
            });
        });

        $(document).ready(function () {
            $("#send-select-all").click(function () {
                $(".send-select").prop('checked', $(this).prop('checked'));
            });
        });
    </script>

@stop