@extends('layouts.fixed')

@section('title','Assign Subject Teachers')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Assign Subject Teacher</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fas fa-tachometer-alt"></i> Home</a></li>
            <li><a href="#">Assign Subject Teacher</a></li>

        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        {{--<button class="btn btn-primary"><a href="{{url('teacher-create')}}" style="color: white;"> Add Assign Subject Teacher</a></button>--}}
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            {{ Form::open(['action' => 'AssignSubjectTeacherController@store','method'=>'post','id' => 'form']) }}
                            {{Form::hidden('teacher_id',$teacher->id)}}
                            {{Form::hidden('batch_id',$batch->id)}}

                            <div class="col-md-12">
                                <label>Teacher Name :</label>
                                <span>{{$teacher->name}}</span>
                            </div>
                            <div class="col-md-12">
                                <label>Joining Date :</label>
                                <span>{{$teacher->join_date}}</span>
                            </div>
                        </div>
                        <br />

                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th class="text-center"><input type="checkbox" name="status" id="checkbox" /></th>
                                <th>Subject Name</th>
                                <th>Course Code</th>
                                <th>Credit Hours</th>
                                {{--<th>Email</th>--}}
                                {{--<th>Password</th>--}}
                                {{--<th>Action</th>--}}
                            </tr>
                            </thead>
                            <tbody>
                            @php $sl=0; @endphp
                            @foreach($subjects as $subject)
                                <tr>
                                    <td class="text-center"><input  type="checkbox" name="subject_id[]" value="{{$subject->id}}" class="checkBoxClass" />
                                        {{--<input type="hidden" name="subject_id[]" value="{{$subject->id}}" />--}}
                                    </td>
                                    <th> {{$subject->name}}</th>
                                    <td> {{$subject->course_code}}</td>
                                    <td> {{$subject->credit}}</td>
                                    {{--                                    <td> {{$teacher->email}}</td>--}}
                                    {{--<td> {{$teacher->password}}</td>--}}
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-10 text-center">
                            <button type="submit" class="btn btn-success" id="teacher_search">Assign Subject Teachers</button>
                        </div>
                    </div>
                    {{Form::close()}}
                    <br>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->

@stop

@section('script')

    <!-- DataTables -->
    <script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>

    <!-- page script -->
    <script>
        $(function () {
            $('#example1').DataTable();
            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : false,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })
        })
    </script>
    <script>
        var as=0;
        $("#checkbox").click(function () {
            $(".checkBoxClass").prop('checked', $(this).prop('checked'));
        });
    </script>
@stop