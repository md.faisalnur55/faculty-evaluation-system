@extends('layouts.fixed')
@section('title','Faculty Evaluation System')
@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> Assign Subject Teacher</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fas fa-tachometer-alt"></i> Home</a></li>
            <li><a href="#">Assign Subject Teacher</a></li>

        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- SELECT2 EXAMPLE -->
        <div class="box box-default">
            <div class="box-header with-border">
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fas fa-times"></i></button>
                </div>
                <br>
            </div>

            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    {{ Form::open(['action' => 'AssignSubjectTeacherController@create','method'=>'post','id' => 'form']) }}
                    <div class="col-md-8 col-md-offset-2">

                        <div class="form-group">
                            {{ Form::label(' Select Trimester:') }}
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fas fa-calendar-alt"></i>
                                </div>
                                {{ Form::select('trimester_id',$trimesters,null,['class'=>'form-control select2','id'=>'trimester_id','placeholder'=>'Select trimester'])}}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label(' Select Batch:') }}
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fas fa-calendar-alt"></i>
                                </div>
                                {{ Form::select('batch_id',$batches,null,['class'=>'form-control select2','id'=>'batch_id','placeholder'=>'Select batch'])}}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label(' Select Department :') }}
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fas fa-calendar-alt"></i>
                                </div>
                                {{ Form::select('department_id',$departments,null,['class'=>'form-control select2','id'=>'department_id','placeholder'=>'Select department'])}}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label(' Select Teacher :') }}
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fas fa-calendar-alt"></i>
                                </div>
{{--                                {{ Form::select('teacher_id',$teachers,null,['class'=>'form-control','id'=>'teacher_id','placeholder'=>'Select Student ID'])}}--}}
                                <select class="form-control" id="teacher_id" name="teacher_id">

                                </select>
                            </div>
                        </div>



                        <div class="form-group row">
                            <div class="col-md-10">
                                <button type="submit" class="btn btn-success" id="teacher_search">Search</button>
                            </div>
                        </div>
                    </div>
                    {{Form::close()}}
                </div>
                <!-- /.col -->
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->

@stop

@section('style')



@stop
@section('script')
    <script>
        $(document).ready(function () {
            $("#teacher_id").select2({
                placeholder:'Select Teacher',
                tags:true
            });
        })
    </script>
    <script>
        $('#department_id').change(function () {
            var department_id = $(this).val();
            var token = "{{csrf_token()}}";

            $.ajax({
                type:'POST',
                url:"{{route('load_teacher')}}",
                data:{department_id:department_id,_token:token},
                success:function(data) {
                    $("#teacher_id").html(data);
                }
            })



        });
    </script>
@stop