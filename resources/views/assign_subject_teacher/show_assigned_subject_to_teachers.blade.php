@extends('layouts.fixed')

@section('title','Faculty Evaluation System')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Assigned Subjects to Teachers</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fas fa-tachometer-alt"></i> Home</a></li>
            <li><a href="#">Assigned Subjects to Teachers</a></li>

        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <button class="btn btn-primary"><a href="{{url('assign-subject-teacher-manage')}}" style="color: white;"> Assign Subjects to Teacher</a></button>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="myTable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>SL NO</th>
                                    <th>Name</th>
                                    <th>Department</th>
                                    <th>Assigned Subjects</th>
                                    {{--<th>Created at</th>--}}
                                    {{--<th>Password</th>--}}
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $i = 1; ?>
                            @foreach($teachers as $teacher)
                                <tr>
                                    <th> {{$i++}}</th>
                                    <td> {{$teacher->name}}</td>
                                    <td> {{$teacher->department->name}}</td>
                                    <td>
                                        <ul>
                                        @foreach($teacher->subjects as $subject)
                                            <li>{{$subject->course_code}} - {{$subject->name}} - {{\App\Batch::query()->findOrFail($subject->pivot->batch_id)->department->name}} {{\App\Batch::query()->findOrFail($subject->pivot->batch_id)->name}}</li>
                                        @endforeach
                                        </ul>
                                    </td>
{{--                                    <td> {{$teacher->trimester->name}}</td>--}}
{{--                                    <td> {{$teacher->created_at->format('D-M-Y')}}</td>--}}
{{--                                    <td> {{$teacher->email}}</td>--}}
                                    {{--<td> {{$teacher->password}}</td>--}}
                                    <td>
                                        <div class="row">
                                            <div class="col-md-3">

                                                <a href="{{url('batch-edit',$teacher->id)}}" class="btn btn-info btn-sm"><i class="fa fa-pencil"></i> Edit</a>
                                            </div>
                                            <div class="col-md-2">
                                                    {{ Form::open(['url'=>['batch-delete',$teacher->id],'method'=>'post']) }}
                                                        <input type="hidden" name="_method" value="DELETE">
                                                    {{ csrf_field()}}
                                                    <button type="submit" class="btn btn-danger btn-sm" ><i class="fa fa-trash-o"></i> Delete</button>
                                                {{Form::close()}}
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->

@stop

@section('script')

    <!-- DataTables -->
    <script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>

    <!-- page script -->
    <script>
        $(function () {
            $('#example1').DataTable();
            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : false,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })
        })
    </script>
@stop