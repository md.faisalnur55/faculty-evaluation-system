@extends('layouts.fixed')

@section('title','Faculty Evaluation System')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Create Subject</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fas fa-tachometer-alt"></i> Home</a></li>
            <li><a href="#">Edit  Subject</a></li>

        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- SELECT2 EXAMPLE -->
        <div class="box box-default">
            <div class="box-header with-border">
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fas fa-times"></i></button>
                </div>
                <br>
            </div>

            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    {{ Form::model($subject,['action' =>['SubjectController@update',$subject->id],'id' => 'form']) }}
                    <div class="col-md-8 col-md-offset-2">
                        <div class="form-group">
                            {{ Form::label(' Name:') }}
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fas fa-calendar-alt"></i>
                                </div>
                                {{ Form::text('name',null,['class'=>'form-control','required'])}}
                            </div>
                        </div>
                        <div class="form-group">
                            {{ Form::label(' Course Code:') }}
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fas fa-calendar-alt"></i>
                                </div>
                                {{ Form::text('course_code',null,['class'=>'form-control','required'])}}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('Credit:') }}
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fas fa-calendar-alt"></i>
                                </div>
                                {{ Form::text('credit',null,['class'=>'form-control','required'])}}
                            </div>
                        </div>
                        <div class="form-group">
                            {{ Form::label(' Department :') }}
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fas fa-calendar-alt"></i>
                                </div>
                                {{ Form::select('department_id',$departments,['class'=>'form-control','placeholder'=>'Select Department'])}}
                            </div>
                        </div>
                        <div class="form-group">
                            {{ Form::label(' Trimester :') }}
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fas fa-calendar-alt"></i>
                                </div>
                                {{ Form::select('trimester_id',$trimesters,['class'=>'form-control','placeholder'=>'Select Department'])}}
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-10">
                                <button type="submit" class="btn btn-success">Update</button>
                            </div>
                        </div>
                    </div>

                    {{Form::close()}}

                </div>
                <!-- /.col -->
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->

@stop

@section('style')



@stop

@section('script')



    <!-- Page script -->
    <script>

    </script>

@stop