<?php

use App\Permission;
use App\PermissionRole;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SeedPermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

//    Routes start


//    Routes end

    public function up()
    {
        Schema::table('permissions', function (Blueprint $table) {
            $permissions = [
                //              Role Permission
                ['name'=>'Manage','permission_key'=>'role-manage','label_id'=>'1'],
                ['name'=>'Create','permission_key'=>'role-create','label_id'=>'1'],
                ['name'=>'Store','permission_key'=>'role/store','label_id'=>'1'],
                ['name'=>'Edit','permission_key'=>'role-edit','label_id'=>'1'],
                ['name'=>'Delete','permission_key'=>'role-delete','label_id'=>'1'],
                //              User Permission
                ['name'=>'Manage','permission_key'=>'user-manage','label_id'=>'2'],
                ['name'=>'Create','permission_key'=>'user-create','label_id'=>'2'],
                ['name'=>'Store','permission_key'=>'user/store','label_id'=>'2'],
                ['name'=>'Edit','permission_key'=>'user-edit','label_id'=>'2'],
                ['name'=>'Delete','permission_key'=>'user-delete','label_id'=>'2'],
                //              Teacher Permission
                ['name'=>'Manage','permission_key'=>'teacher-manage','label_id'=>'3'],
                ['name'=>'Create','permission_key'=>'teacher-create','label_id'=>'3'],
                ['name'=>'Store','permission_key'=>'teacher/store','label_id'=>'3'],
                ['name'=>'Edit','permission_key'=>'teacher-edit','label_id'=>'3'],
                ['name'=>'Delete','permission_key'=>'teacher-delete','label_id'=>'3'],
                //              Student Permission
                ['name'=>'Manage','permission_key'=>'student-manage','label_id'=>'4'],
                ['name'=>'Create','permission_key'=>'student-create','label_id'=>'4'],
                ['name'=>'Store','permission_key'=>'student/store','label_id'=>'4'],
                ['name'=>'Edit','permission_key'=>'student-edit','label_id'=>'4'],
                ['name'=>'Delete','permission_key'=>'student-delete','label_id'=>'4'],
                //                  Settings
                //              Department Permission
                ['name'=>'Manage','permission_key'=>'department-manage','label_id'=>'5'],
                ['name'=>'Create','permission_key'=>'department-create','label_id'=>'5'],
                ['name'=>'Store','permission_key'=>'department/store','label_id'=>'5'],
                ['name'=>'Edit','permission_key'=>'department-edit','label_id'=>'5'],
                ['name'=>'Delete','permission_key'=>'department-delete','label_id'=>'5'],
                //              Batch Permission
                ['name'=>' Manage','permission_key'=>'batch-manage','label_id'=>'6'],
                ['name'=>' Create','permission_key'=>'batch-create','label_id'=>'6'],
                ['name'=>' Store','permission_key'=>'batch/store','label_id'=>'6'],
                ['name'=>' Edit','permission_key'=>'batch-edit','label_id'=>'6'],
                ['name'=>' Delete','permission_key'=>'batch-delete','label_id'=>'6'],
                //              Trimester Permission
                ['name'=>'Manage','permission_key'=>'trimester-manage','label_id'=>'7'],
                ['name'=>'Create','permission_key'=>'trimester-create','label_id'=>'7'],
                ['name'=>'Store','permission_key'=>'trimester/store','label_id'=>'7'],
                ['name'=>'Edit','permission_key'=>'trimester-edit','label_id'=>'7'],
                ['name'=>'Delete','permission_key'=>'trimester-delete','label_id'=>'7'],
                //              Subject Permission
                ['name'=>' Manage','permission_key'=>'subject-manage','label_id'=>'8'],
                ['name'=>' Create','permission_key'=>'subject-create','label_id'=>'8'],
                ['name'=>' Store','permission_key'=>'subject/store','label_id'=>'8'],
                ['name'=>' Edit','permission_key'=>'subject-edit','label_id'=>'8'],
                ['name'=>' Delete','permission_key'=>'subject-delete','label_id'=>'8'],
                //              Question Permission
                ['name'=>' Manage','permission_key'=>'question-manage','label_id'=>'9'],
                ['name'=>' Create','permission_key'=>'question-create','label_id'=>'9'],
                ['name'=>' Store','permission_key'=>'question/store','label_id'=>'9'],
                ['name'=>' Edit','permission_key'=>'question-edit','label_id'=>'9'],
                ['name'=>' Delete','permission_key'=>'question-delete','label_id'=>'9'],
                //              Assign Subject Permission
                ['name'=>'Manage','permission_key'=>'assign-subject-manage','label_id'=>'10'],
                ['name'=>'Create','permission_key'=>'assign-subject-create','label_id'=>'10'],
                ['name'=>'Store','permission_key'=>'assign-subject/store','label_id'=>'10'],
                ['name'=>'Edit','permission_key'=>'assign-subject-edit','label_id'=>'10'],
                ['name'=>'Delete','permission_key'=>'assign-subject-delete','label_id'=>'10'],
                //              Assign Subject to Teacher Permission
                ['name'=>'Manage','permission_key'=>'assign-subject-teacher-manage','label_id'=>'11'],
                ['name'=>'Create','permission_key'=>'assign-subject-teacher-create','label_id'=>'11'],
                ['name'=>'Store','permission_key'=>'assign-subject-teacher/store','label_id'=>'11'],
                ['name'=>'Edit','permission_key'=>'assign-subject-teacher-edit','label_id'=>'11'],
                ['name'=>'Delete','permission_key'=>'assign-subject-teacher-delete','label_id'=>'11'],
                //              Evaluation Permission
                ['name'=>'Manage','permission_key'=>'evaluation-manage','label_id'=>'12'],
                ['name'=>'Create','permission_key'=>'evaluation-create','label_id'=>'12'],
                ['name'=>'Store','permission_key'=>'evaluation/store','label_id'=>'12'],
                ['name'=>'Edit','permission_key'=>'evaluation-edit','label_id'=>'12'],
                ['name'=>'Delete','permission_key'=>'evaluation-delete','label_id'=>'12'],
                ['name'=>'Search','permission_key'=>'evaluation-search','label_id'=>'12'],
                ['name'=>'View Total Percentage','permission_key'=>'evaluation-total-search','label_id'=>'12'],

                //              File Upload Permission
                ['name'=>'Manage','permission_key'=>'file-upload-manage','label_id'=>'13'],
                ['name'=>'Create','permission_key'=>'file-upload-create','label_id'=>'13'],
                ['name'=>'Store','permission_key'=>'file-upload/store','label_id'=>'13'],
                ['name'=>'Edit','permission_key'=>'file-upload-edit','label_id'=>'13'],
                ['name'=>'Delete','permission_key'=>'file-upload-delete','label_id'=>'13'],
                //              Course Material Permission
                ['name'=>'Course Material Manage','permission_key'=>'course-material-manage','label_id'=>'14'],
                ['name'=>'Course Material Create','permission_key'=>'course-material-search','label_id'=>'14'],
                //              Course Material Permission
                ['name'=>'Faculty Result Send','permission_key'=>'send-faculty-evaluation-result-search','label_id'=>'15'],
                ['name'=>'View Evaluation Percentage','permission_key'=>'evaluation-search','label_id'=>'15']
            ];
            foreach ($permissions as $permission){
                $p = Permission::query()->create($permission);
                $data =[];
                $data['role_id'] = 1;
                $data['permission_id'] = $p->id;
                PermissionRole::query()->create($data);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permissions', function (Blueprint $table) {
            //
        });
    }
}
