<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeignEvaluationStudentTeacherSubjectBatchId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('evaluations', function (Blueprint $table) {
            $table->foreign('subject_id')->references('id')->on('subjects');
            $table->foreign('trimester_id')->references('id')->on('trimesters');
            $table->foreign('teacher_id')->references('id')->on('teachers');
            $table->foreign('student_id')->references('id')->on('students');
            $table->foreign('batch_id')->references('id')->on('batches');
            $table->foreign('question_id')->references('id')->on('questions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('evaluations', function (Blueprint $table) {
            $table->dropForeign(['subject_id']);
            $table->dropForeign(['trimester_id']);
            $table->dropForeign(['teacher_id']);
            $table->dropForeign(['student_id']);
            $table->dropForeign(['batch_id']);
            $table->dropForeign(['question_id']);
        });
    }
}
