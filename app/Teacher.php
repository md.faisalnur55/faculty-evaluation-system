<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Teacher extends Model
{
    protected $table='teachers';
    protected $fillable=['department_id','name','f_name','m_name','dob','join_date','email'];

    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    public function subjects()
    {
        return $this->belongsToMany(Subject::class,'subject_teacher')->withPivot('batch_id');
    }

    public function evaluations()
    {
        return $this->hasMany(Teacher::class);
    }
}
