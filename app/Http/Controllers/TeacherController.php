<?php

namespace App\Http\Controllers;

use App\Department;
use App\Role;
use App\Teacher;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class TeacherController extends Controller
{
    public function index()
    {
        $teachers = Teacher::all();
        return view('teacher.index',compact('teachers'));
    }

    public function create()
    {
        $this->checkpermission('teacher-create');
        $roles = Role::query()->pluck('name','id');
        $departments = Department::query()->pluck('name','id');
        return view('teacher.create',compact('roles','departments'));
    }

    public function store(Request $request)
    {
//        dd($request->all());
        $password = mt_rand(100000, 999999);
        $user = new User();
        $user->email = $request->email;
        $user->name = $request->name;
        $user->username = $request->name;
        $user->status = '1';
        $user->password = bcrypt($password);
        $user->save();
        $user->roles()->attach($request->role_id);

        $student = new Teacher();
        $student->user_id = $user->id;
        $student->department_id = $request->department_id;
        $student->name = $request->name;
        $student->f_name = $request->f_name;
        $student->m_name = $request->m_name;
        $student->dob = $request->dob;
        $student->join_date = $request->join_date;
        $student->email = $request->email;
        $student->save();


        $email = $request->email;
        $name = $user->name;
        //Mail start
        Mail::send('admin_mail.send_password', compact('password','name'), function($message) use ($email)  {
            $message->to($email , 'portcity.edu.bd')
                ->subject('Faculty Evaluation System Credentials');
            $message->from('faisalnurroney@gmail.com','Faculty Evaluation Result');
        });
        //Mail ends


//        Teacher::query()->create($request->all());
        return redirect('teacher-manage');
    }

    public function edit($id)
    {
        $this->checkpermission('teacher-edit');
        $roles = Role::query()->pluck('name','id');
        $departments = Department::query()->pluck('name','id');
        $teacher = Teacher::query()->findOrFail($id);
        return view('teacher.edit',compact('teacher','roles','departments'));
    }

    public function update(Request $request, $id)
    {
        $teacher = Teacher::query()->findOrFail($id);
        $user = User::query()->findOrFail($teacher->user_id);
        $user->roles()->sync($request->role_id);
        $teacher->update($request->all());
        return redirect('teacher-manage');
    }

    public function destroy($id)
    {
        $this->checkpermission('teacher-delete');
        $teacher = Teacher::query()->findOrFail($id);
        $user = User::query()->findOrFail($teacher->user_id);
        $teacher->delete();
        $user->roles()->detach($user->id);
        $user->delete();

        return redirect('teacher-manage');
    }
}
