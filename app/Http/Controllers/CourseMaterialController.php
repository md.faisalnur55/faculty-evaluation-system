<?php

namespace App\Http\Controllers;

use App\FileUpload;
use App\Trimester;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CourseMaterialController extends Controller
{
    public function index(Request $request)
    {
//        dd($request->all());
        $this->checkpermission('course-material-manage');
        $file_uploads = FileUpload::query()
            ->where('trimester_id',$request->trimester_id)
            ->where('batch_id',Auth::user()->student->batch_id)
            ->where('subject_id',$request->subject_id)
            ->get();
        return view('course_material.index',compact('file_uploads'));
    }

    public function search()
    {
        $this->checkpermission('course-material-search');
        $trimesters = Trimester::query()->pluck('name','id');
        return view('course_material.create',compact('trimesters'));
    }

//    AJAX FUNCTIONS

    public function load_course_material_subjects(Request $request)
    {
        $html ="";
        if(Auth::user()->student){
            $trimester_id = $request->trimester_id;
            $department_id = Auth::user()->student->department_id;
            $subjects = Auth::user()->student->subjects->where('trimester_id',$trimester_id);

            foreach($subjects as $subject){
                $html.="<option value='{$subject->id}'>{$subject->name}</option>";
            }
        }
        return $html;
    }
}
