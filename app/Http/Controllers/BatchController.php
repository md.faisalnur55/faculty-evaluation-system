<?php

namespace App\Http\Controllers;

use App\Batch;
use App\Department;
use Illuminate\Http\Request;

class BatchController extends Controller
{
    public function index()
    {
        $batches = Batch::all();
        return view('batch.index',compact('batches'));
    }

    public function create()
    {
        $this->checkpermission('batch-create');
        $departments = Department::query()->pluck('name','id');
        return view('batch.create',compact('departments'));
    }

    public function store(Request $request)
    {
//        dd($request->all());
        Batch::query()->create($request->all());
        return redirect('batch-manage');
    }

    public function edit($id)
    {
        $this->checkpermission('batch-edit');
        $departments = Department::query()->pluck('name','id');
        $batch = Batch::query()->findOrFail($id);
        return view('batch.edit',compact('batch','departments'));
    }

    public function update(Request $request, $id)
    {
        $batch = Batch::query()->findOrFail($id);
        $batch->update($request->all());
        return redirect('batch-manage');
    }

    public function destroy($id)
    {
        $this->checkpermission('batch-delete');
        $batch = Batch::query()->findOrFail($id);
        $batch->delete();
        return redirect('batch-manage');
    }
}
