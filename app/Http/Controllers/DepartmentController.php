<?php

namespace App\Http\Controllers;

use App\Department;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;

class DepartmentController extends Controller
{
    public function index()
    {
        $this->checkpermission('department-manage');
        $departments = Department::all();
        return view('department.index',compact('departments'));
    }

    public function create()
    {
        $this->checkpermission('department-create');
        return view('department.create');
    }

    public function store(Request $request)
    {
        Department::query()->create($request->all());
        return redirect('department-manage');
    }

    public function edit($id)
    {
        $this->checkpermission('department-edit');
        $department = Department::query()->findOrFail($id);
        return view('department.edit',compact('department'));
    }

    public function update(Request $request, $id)
    {
        $department = Department::query()->findOrFail($id);
        $department->update($request->all());
        return redirect('department-manage');
    }

    public function destroy($id)
    {
        $this->checkpermission('department-delete');
        $department = Department::query()->findOrFail($id);
        $department->delete();
        return redirect('department-manage');
    }
}
