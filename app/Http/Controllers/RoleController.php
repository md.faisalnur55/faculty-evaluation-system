<?php

namespace App\Http\Controllers;

use App\Permission;
use App\PermissionRole;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
       $roles=Role::all();
        return view('role.index',compact('roles'));
    }
    public function create(){
        $this->checkpermission('role-create');
        $roles = Role::all();
//        $roles = Role::pluck('name','id');
        $permissions = Permission::all();

        $role_permission        = Permission::query()->where('label_id','1')->get();
        $user                   = Permission::query()->where('label_id','2')->get();
        $teacher                = Permission::query()->where('label_id','3')->get();
        $student                = Permission::query()->where('label_id','4')->get();
        $department             = Permission::query()->where('label_id','5')->get();
        $batch                  = Permission::query()->where('label_id','6')->get();
        $trimester              = Permission::query()->where('label_id','7')->get();
        $subject                = Permission::query()->where('label_id','8')->get();
        $question               = Permission::query()->where('label_id','9')->get();
        $assign_subject         = Permission::query()->where('label_id','10')->get();
        $assign_subject_teacher = Permission::query()->where('label_id','11')->get();
        $evaluation             = Permission::query()->where('label_id','12')->get();
        $file_upload            = Permission::query()->where('label_id','13')->get();
        $course_material        = Permission::query()->where('label_id','14')->get();
        $send_result            = Permission::query()->where('label_id','15')->get();

        return view('role.add-role',compact('roles','permissions','send_result','user','role_permission','teacher','student','department','batch','trimester','subject','question','assign_subject','assign_subject_teacher','evaluation','file_upload','course_material'));
    }
    public function store(Request $request){

//        dd($request->all());
        $role = Role::query()->create($request->all());
        $data = [];
        $permissions = $request->asignpermission;
        foreach($permissions as $key => $permission){
            $data=[
                'role_id' => $role->id,
                'permission_id' => $permission
            ];
            PermissionRole::query()->create($data);
        }
        return redirect('role-create');
    }
    public function edit($id){
        $this->checkpermission('role-edit');
        $roles = Role::all();
        $role = Role::query()->findOrFail($id);

        $permissions = Permission::all();
        $currentpermissions = $role->permissions;
        $check = '';

        $role_permission        = Permission::query()->where('label_id','1')->get();
        $user                   = Permission::query()->where('label_id','2')->get();
        $teacher                = Permission::query()->where('label_id','3')->get();
        $student                = Permission::query()->where('label_id','4')->get();
        $department             = Permission::query()->where('label_id','5')->get();
        $batch                  = Permission::query()->where('label_id','6')->get();
        $trimester              = Permission::query()->where('label_id','7')->get();
        $subject                = Permission::query()->where('label_id','8')->get();
        $question               = Permission::query()->where('label_id','9')->get();
        $assign_subject         = Permission::query()->where('label_id','10')->get();
        $assign_subject_teacher = Permission::query()->where('label_id','11')->get();
        $evaluation             = Permission::query()->where('label_id','12')->get();
        $file_upload            = Permission::query()->where('label_id','13')->get();
        $course_material        = Permission::query()->where('label_id','14')->get();
        $send_result            = Permission::query()->where('label_id','15')->get();

        return view('role.edit',compact('role','role_permission','send_result','user','permissions','roles','currentpermissions','check','teacher','student','department','batch','trimester','subject','question','assign_subject','assign_subject_teacher','evaluation','file_upload','course_material'));
    }
    public function update($id, Request $request)
    {
//        dd($request->all());
        $role = Role::query()->findOrFail($id);
        $role->update($request->all());
        $role->permissions()->sync($request->asignpermission);

        return redirect('role-create');
    }
    public function destroy($id){

        $this->checkpermission('role-delete');
        $role = Role::query()->findOrFail($id);
        $role->permissions()->detach();
        $role->delete();
        session()->flash('success', 'Role has store successfully');
        return redirect()->route('role.create');
      }

}
