<?php

namespace App\Http\Controllers;

use App\Trimester;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TrimesterController extends Controller
{
    public function index()
    {
        $trimesters = Trimester::all();
        return view('trimester.index',compact('trimesters'));
    }

    public function create()
    {
        $this->checkpermission('trimester-create');
        return view('trimester.create');
    }

    public function store(Request $request)
    {
        Trimester::query()->create($request->all());
        return redirect('trimester-manage');
    }

    public function edit($id)
    {
        $this->checkpermission('trimester-edit');
        $trimester = Trimester::query()->findOrFail($id);
        return view('trimester.edit',compact('trimester'));
    }

    public function update(Request $request, $id)
    {
        $trimester = Trimester::query()->findOrFail($id);
        $trimester->update($request->all());
        return redirect('trimester-manage');
    }

    public function destroy($id)
    {
        $this->checkpermission('trimester-delete');
        $trimester = Trimester::query()->findOrFail($id);
        $trimester->delete();
        return redirect('trimester-manage');
    }
}
