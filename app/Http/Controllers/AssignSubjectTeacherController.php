<?php

namespace App\Http\Controllers;

use App\Batch;
use App\Department;
use App\Subject;
use App\SubjectTeacher;
use App\Teacher;
use App\Trimester;
use Illuminate\Http\Request;

class AssignSubjectTeacherController extends Controller
{
    public function index()
    {
        $this->checkpermission('assign-subject-teacher-manage');
        $trimesters = Trimester::query()->pluck('name','id');
        $departments = Department::query()->pluck('name','id');
        $batches = Batch::query()->pluck('name','id');
        $teachers =  Teacher::query()->pluck('name','id');
        return view('assign_subject_teacher.index',compact('teachers','batches','departments','trimesters'));
    }

    public function create(Request $request)
    {
        $this->checkpermission('assign-subject-teacher-create');
        $batch = Batch::query()->findOrFail($request->batch_id);
        $teacher =  Teacher::query()->findOrFail($request->teacher_id);
        $subjects = Subject::query()
            ->where('department_id',$request->department_id)
            ->where('trimester_id',$request->trimester_id)
            ->get();
        return view('assign_subject_teacher.create',compact('teacher','subjects','batch'));
    }

    public function store(Request $request)
    {
        foreach($request->subject_id as $subject){
            $check = SubjectTeacher::query()
                ->where('batch_id',$request->batch_id)
                ->where('subject_id',$subject)
                ->count();
            if($check == 0){
                $subject_teacher = new SubjectTeacher();
                $subject_teacher['teacher_id'] = $request->teacher_id;
                $subject_teacher['batch_id']=$request->batch_id;
                $subject_teacher['subject_id']= $subject;
                $subject_teacher->save();
            }
        }

        $trimesters = Trimester::query()->pluck('name','id');
        $departments = Department::query()->pluck('name','id');
        $teachers =  Teacher::query()->pluck('name','id');
        $batches = Batch::query()->pluck('name','id');
        return view('assign_subject_teacher.index',compact('teachers','departments','trimesters','batches'));
    }



    public function show()
    {
        $teachers = Teacher::all();
        return view('assign_subject_teacher.show_assigned_subject_to_teachers',compact('teachers'));
    }


//    AJAX METHODS
    public function load_teacher(Request $request)
    {
        $teachers = Teacher::query()->where('department_id',$request->department_id)->get();
        $html ="<option>Select Teacher</option>";
        foreach ($teachers as $teacher) {

            $html.= "<option value='{$teacher->id}']>{$teacher->name}</option>";
        }
        return $html;
    }
}
