<?php

namespace App\Http\Controllers;

use App\Batch;
use App\Department;
use App\FileUpload;
use App\Subject;
use App\SubjectTeacher;
use App\Trimester;
use Faker\Provider\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class FileUploadController extends Controller
{
    public function index()
    {
        $this->checkpermission('file-upload-manage');
        if(Auth::user()->teacher !=null){
            $file_uploads = FileUpload::query()->where('teacher_id',Auth::user()->teacher->id)->get();
            return view('file_upload.index',compact('file_uploads'));
        }
        return redirect()->back();

    }

    public function create()
    {
        $this->checkpermission('file-upload-create');
        $departments = Department::pluck('name','id');
        $trimesters = Trimester::pluck('name','id');
        return view('file_upload.create',compact('departments','trimesters'));
    }

    public function store(Request $request)
    {
        $teacher_id = Auth::user()->teacher->id;
        if($teacher_id){
            $data = $request->except('file_name');
            if($request->hasFile('file_name')){
                $image = $request->file('file_name');
                $image_name = substr(md5(time()),0,6);
                $filename = Date('Y').'_'.$image_name.".".$image->getClientOriginalExtension();
                $image->move(base_path('public/uploads/'),$filename);
                $data['file_name']= $filename;
                $data['teacher_id']= $teacher_id;
                FileUpload::query()->create($data);
                Session::flash('success','File Uploaded successfully.');
            }
        }

        return redirect('file-upload-manage');
    }

    public function edit($id)
    {
        $departments = Department::pluck('name','id');
        $trimesters = Trimester::pluck('name','id');
        $file_upload = FileUpload::query()->findOrFail($id);
        return view('file_upload.edit',compact('file_upload','departments','trimesters'));
    }

    public function update(Request $request)
    {
        $file_upload = FileUpload::query()->find($request->id);

        /* image remove */


        if( $request->hasFile('image'))
        {
            /* old image unlink start */
            $path = 'public/uploads/'.$file_upload->file_name;
            unlink($path);
            /* old image unlink end */
            $image = $request->file('image');
            $image_name = substr(md5(time()),0,6);
            $filename = Date('Y').'_'.$image_name.".".$image->getClientOriginalExtension();
            //$file_upload->image = $filename;

            $image->move(base_path('public/uploads/'),$filename);
            $data = $request->except('file_name');
            $data['file_name'] = $filename;
            $file_upload->update($data);
        }else{
            $file_upload->update($request->except('file_name'));
        }

        session()->flash('success','Updated successfully');
        return redirect('file-upload-manage');
    }

    public function destroy($id)
    {
        $file_upload = FileUpload::query()->findOrFail($id);
        $file_upload->delete();
        return redirect('file-upload-manage');
    }

//    AJAX Functions start
    public function load_batches(Request $request)
    {
        $batches = Batch::query()->where('department_id',$request->department_id)->get();
        $html = "<option>Select Batch</option>";
        foreach ($batches as $batch){
            $html.="<option value='{$batch->id}'>{$batch->name}</option>";
        }
        return $html;
    }

    public function load_subjects(Request $request)
    {
        $teacher_id = Auth::user()->teacher;
        if($teacher_id){
            $subjects = SubjectTeacher::query()->where('batch_id',$request->batch_id)
                ->where('teacher_id',$teacher_id->id)
                ->get();
        }
        $html = "<option>Select Subject</option>";
        foreach ($subjects as $subject){
            $subject_name = Subject::query()->findOrFail($subject->subject_id);
            $html.="<option value='{$subject_name->id}'>{$subject_name->name}</option>";
        }
        return $html;
    }
    //    AJAX Functions end
}
