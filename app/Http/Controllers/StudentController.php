<?php

namespace App\Http\Controllers;

use App\Batch;
use App\Department;
use App\Role;
use App\RoleUser;
use App\Student;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class StudentController extends Controller
{
    public function index()
    {
        $students = Student::all();
        return view('student.index',compact('students'));
    }

    public function create()
    {
        $this->checkpermission('student-create');
        $batches =Batch::query()->pluck('name','id');
        $roles = Role::query()->pluck('name','id');
        $departments = Department::query()->pluck('name','id');
        return view('student.create',compact('departments','roles','batches'));
    }

    public function store(Request $request)
    {
        $password = mt_rand(100000, 999999);
        $user = new User();
        $user->email = $request->email;
        $user->name = $request->name;
        $user->username = $request->name;
        $user->status = '1';
        $user->password = bcrypt($password);
        $user->save();
        $user->roles()->attach($request->role_id);

        $student = new Student();
        $student->user_id = $user->id;
        $student->department_id = $request->department_id;
        $student->batch_id = $request->batch_id;
        $student->phone = $request->phone;
        $student->name = $request->name;
        $student->f_name = $request->f_name;
        $student->m_name = $request->m_name;
        $student->dob = $request->dob;
        $student->email = $request->email;
        $student->card_id = $request->card_id;
        $student->save();



        $email = $request->email;
        $name = $user->name;
        //Mail start
        Mail::send('admin_mail.send_password', compact('password','name'), function($message) use ($email)  {
            $message->to($email , 'portcity.edu.bd')
                ->subject('Faculty Evaluation System Credentials');
            $message->from('faisalnurroney@gmail.com','Faculty Evaluation Result');
        });
        //Mail ends



        return redirect('student-manage');
    }

    public function edit($id)
    {
        $this->checkpermission('student-edit');
        $batches =Batch::query()->pluck('name','id');
        $roles = Role::query()->pluck('name','id');
        $departments = Department::query()->pluck('name','id');
        $student = Student::query()->findOrFail($id);
        return view('student.edit',compact('student','departments','roles','batches'));
    }

    public function update(Request $request, $id)
    {
        $student = Student::query()->findOrFail($id);
        $user = User::query()->findOrFail($student->user_id);
        $user->roles()->sync($request->role_id);
        $student->update($request->all());

        return redirect('student-manage');
    }

    public function destroy($id)
    {
        $this->checkpermission('student-delete');
        $student = Student::query()->findOrFail($id);
        $user = User::query()->findOrFail($student->user_id);
        $student->delete();
        $user->roles()->detach($user->id);
        $user->delete();
        return redirect('student-manage');
    }

    public function loadStudentCardId(Request $request)
    {
        $batch_id       = $request->batch_id;
        $department_id  = $request->department_id;
        $student_count  = Student::query()->where('batch_id',$batch_id)->where('department_id',$department_id)->count();
        $card_id        = Department::query()->findOrFail($department_id)->name.Batch::query()->findOrFail($batch_id)->name.sprintf('%05d',++$student_count);
        return $card_id;
    }
}
