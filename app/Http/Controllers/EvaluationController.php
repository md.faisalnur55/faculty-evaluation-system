<?php

namespace App\Http\Controllers;

use App\Batch;
use App\Department;
use App\Evaluation;
use App\Question;
use App\Subject;
use App\SubjectTeacher;
use App\Teacher;
use App\Trimester;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EvaluationController extends Controller
{
    public function index()
    {
        $this->checkpermission('evaluation-manage');
        if(Auth::user()->student){
            $subjects= Auth::user()->student->subjects->pluck('name','id');
        }else{
            $subjects = [];
        }
        $questions = Question::all();
        $evaluations = Evaluation::all();
        return view('evaluation.index',compact('evaluations','subjects','questions'));
    }

    public function create()
    {
        $this->checkpermission('evaluation-create');
        $departments = Department::query()->pluck('name','id');
        return view('evaluation.create',compact('departments'));
    }

    public function store(Request $request)
    {
//        dd($request->all());
        $data=[];
        $question_marks = array_combine($request->question_id, $request->marks);
        foreach($question_marks as $question => $mark){
            $data = new Evaluation();
            $data['question_id'] = intval($question);
            $data['marks']= $mark;
            $data['batch_id']= Auth::user()->student->batch_id;
            $data['subject_id']= $request->subject_id;
            $data['trimester_id']= $request->trimester_id;
            $data['student_id']= Auth::user()->student->id;
            $data['teacher_id']= $request->teacher_id;

            $check = Evaluation::query()
                ->where('question_id',$question)
                ->where('batch_id',Auth::user()->student->batch_id)
                ->where('subject_id',$request->subject_id)
                ->where('student_id',Auth::user()->student->id)
                ->where('teacher_id',$request->teacher_id)
                ->where('trimester_id',$request->trimester_id)
                ->first();
            if($check == null){
                $data->save();
            }
        }

//        Evaluation::query()->create($request->all());
        return redirect('evaluation-manage');
    }

    public function edit($id)
    {
        $departments = Department::query()->pluck('name','id');
        $evaluation = Evaluation::query()->findOrFail($id);
        return view('evaluation.edit',compact('evaluation','departments'));
    }

    public function update(Request $request, $id)
    {
        $evaluation = Evaluation::query()->findOrFail($id);
        $evaluation->update($request->all());
        return redirect('evaluation-manage');
    }

    public function destroy($id)
    {
        $evaluation = Evaluation::query()->findOrFail($id);
        $evaluation->delete();
        return redirect('evaluation-manage');
    }

    public function search()
    {
        $departments = Department::query()->pluck('name','id');
        $trimesters = Trimester::query()->pluck('name','id');
        $batches = Batch::query()->pluck('name','id');
        return view('evaluation.teacher-evaluation-search',compact('departments','trimesters','batches'));
    }

    public function view(Request $request)
    {
//        dd($request->all());
        $total = Evaluation::query()
            ->where('teacher_id',$request->teacher_id)
            ->where('subject_id',$request->subject_id)
            ->where('batch_id',$request->batch_id)
            ->sum('marks');
        $count = Evaluation::query()
            ->where('teacher_id',$request->teacher_id)
            ->where('subject_id',$request->subject_id)
            ->where('batch_id',$request->batch_id)
            ->count();

        $eva_number = $count*6;
        $teacher = Teacher::query()->findOrFail($request->teacher_id);

        if($count>0){
            $percentage = ($total/$eva_number)*100;

            $subject = Subject::query()->findOrFail($request->subject_id);
            $batch = Batch::query()->findOrFail($request->batch_id);
            return view('evaluation.teacher-evaluation-view',compact('percentage','teacher','subject','batch'));
        }else{
            return redirect()->back();
        }

    }

    public function total_search()
    {
        $trimesters=Trimester::query()->pluck('name','id');
        $departments=Department::query()->pluck('name','id');
        return view('evaluation.total-evaluation-search',compact('departments','trimesters'));
    }

    public function total_evaluation_view(Request $request)
    {

        $teacher = Teacher::query()->findOrFail($request->teacher_id);
        $trimester = Trimester::query()->findOrFail($request->trimester_id);
        $cum_avg =0;
        $subjects = Evaluation::query()
            ->where('teacher_id',$request->teacher_id)
            ->where('trimester_id',$request->trimester_id)
            ->groupBy('subject_id')
            ->get();
        $subject_count = $subjects->count();
        foreach ($subjects as $subject) {
            $sum = Evaluation::query()
                ->where('subject_id',$subject->subject_id)
                ->where('teacher_id',$request->teacher_id)
                ->where('trimester_id',$request->trimester_id)
                ->sum('marks');

            $count = Evaluation::query()
                ->where('subject_id',$subject->subject_id)
                ->where('teacher_id',$request->teacher_id)
                ->where('trimester_id',$request->trimester_id)
                ->count();
            $avg=$sum/$count;
            $percentage = ($avg/6)*100;
            $cum_avg+=$percentage;
        }
        if($subject_count>0){
            $trimester_final_result = $cum_avg/$subject_count;
        }else{
            session()->flash('success','The faculty memeber is not evaluated for this trimester yet.');
            return redirect()->back();
        }
        return view('evaluation.total-evaluation-view',compact('trimester_final_result','teacher','trimester'));
    }

//    AJAX functions

    public function load_teacher(Request $request)
    {
        $teacher_id = SubjectTeacher::query()
            ->where('subject_id',$request->subject_id)
            ->where('batch_id',Auth::user()->student->batch_id)
            ->first()->teacher_id;
        $teacher = Teacher::query()->findOrFail($teacher_id);
        $trimester_id = Subject::query()->findOrFail($request->subject_id)->trimester->id;

        return [$teacher->id,$teacher->name,$trimester_id];
    }



    public function load_batch(Request $request)
    {
        $html ="<option>Select Batch</option>";
        $teachers = SubjectTeacher::query()
            ->where('teacher_id',$request->teacher_id)
            ->groupBy('batch_id')
            ->get();
        foreach ($teachers as $teacher){
            $batch = Batch::query()->findOrFail($teacher->batch_id);
            $html.="
                <option value='{$batch->id}'>{$batch->name}</option>
            ";
        }
        return $html;
    }


    public function load_subject(Request $request)
    {
        $batches = SubjectTeacher::query()
            ->where('teacher_id',$request->teacher_id)
            ->where('batch_id',$request->batch_id)
            ->get();

        $html = "";

        foreach ($batches as $batch){
            $subject = Subject::query()->findOrFail($batch->subject_id);
            $html.="
                <option value='{$subject->id}'>{$subject->name}</option>
            ";
        }
        return $html;
    }
}
