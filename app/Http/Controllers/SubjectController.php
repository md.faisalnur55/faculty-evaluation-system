<?php

namespace App\Http\Controllers;

use App\Department;
use App\Subject;
use App\Trimester;
use Illuminate\Http\Request;

class SubjectController extends Controller
{
    public function index()
    {
        $subjects = Subject::all();
        return view('subject.index',compact('subjects'));
    }

    public function create()
    {
        $this->checkpermission('subject-create');
        $departments = Department::query()->pluck('name','id');
        $trimesters = Trimester::query()->pluck('name','id');
        return view('subject.create',compact('departments','trimesters'));
    }

    public function store(Request $request)
    {
//        dd($request->all());
        Subject::query()->create($request->all());
//        return redirect('subject-manage');
        return redirect()->back();
    }

    public function edit($id)
    {
        $this->checkpermission('subject-edit');
        $departments = Department::query()->pluck('name','id');
        $trimesters = Trimester::query()->pluck('name','id');
        $subject = Subject::query()->findOrFail($id);
        return view('subject.edit',compact('subject','departments','trimesters'));
    }

    public function update(Request $request, $id)
    {
        $subject = Subject::query()->findOrFail($id);
        $subject->update($request->all());
        return redirect('subject-manage');
    }

    public function destroy($id)
    {
        $this->checkpermission('subject-delete');
        $subject = Subject::query()->findOrFail($id);
        $subject->delete();
        return redirect('subject-manage');
    }
}
