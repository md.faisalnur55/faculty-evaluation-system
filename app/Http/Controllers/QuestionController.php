<?php

namespace App\Http\Controllers;

use App\Question;
use Illuminate\Http\Request;

class 
QuestionController extends Controller
{
    public function index()
    {
        $questions = Question::all();
        return view('question.index',compact('questions'));
    }

    public function create()
    {
        $this->checkpermission('question-create');
        return view('question.create');
    }

    public function store(Request $request)
    {
//        dd($request->all());
        Question::query()->create($request->all());
        return redirect('question-manage');
    }

    public function edit($id)
    {
        $this->checkpermission('question-edit');
        $question = Question::query()->findOrFail($id);
        return view('question.edit',compact('question'));
    }

    public function update(Request $request, $id)
    {
        $question = Question::query()->findOrFail($id);
        $question->update($request->all());
        return redirect('question-manage');
    }

    public function destroy($id)
    {
        $this->checkpermission('question-delete');
        $question = Question::query()->findOrFail($id);
        $question->delete();
        return redirect('question-manage');
    }
}
