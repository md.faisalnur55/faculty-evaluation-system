<?php

namespace App\Http\Controllers;

use App\Batch;
use App\Department;
use App\Student;
use App\StudentSubject;
use App\Subject;
use App\Trimester;
use Illuminate\Http\Request;

class AssignSubjectController extends Controller
{
    public function index()
    {
        $this->checkpermission('assign-subject-manage');
        $trimesters = Trimester::query()->pluck('name','id');
        $departments = Department::query()->pluck('name','id');
        $batches = Batch::query()->pluck('name','id');
        $students =  Student::query()->pluck('card_id','id');
        return view('assign_subject.index',compact('students','batches','departments','trimesters'));
    }

    public function create(Request $request)
    {
//        dd($request->all());
        $this->checkpermission('assign-subject-create');
        $student =  Student::query()->findOrFail($request->student_id);
        $subjects = Subject::query()
            ->where('department_id',$student->department->id)
            ->where('trimester_id',$request->trimester_id)
            ->get();
        return view('assign_subject.create',compact('student','subjects'));
    }

    public function store(Request $request)
    {
//        dd($request->all());
        foreach ($request->subject_id as $subject) {
            $check = StudentSubject::query()
                ->where('student_id',$request->student_id)
                ->where('subject_id',$subject)
                ->count();
            if($check>0){

            }else{
                $student = new StudentSubject();
                $student['student_id']= $request->student_id;
                $student['subject_id']= $subject;
                $student->save();
            }
        }
        $trimesters = Trimester::query()->pluck('name','id');
        $departments = Department::query()->pluck('name','id');
        $batches = Batch::query()->pluck('name','id');
        $students =  Student::query()->pluck('card_id','id');
        return view('assign_subject.index',compact('students','batches','departments','trimesters'));
    }
//
//    public function edit($id)
//    {
//        $student =  Student::query()->findOrFail($id);
//        $subjects = Subject::query()
//            ->where('department_id',$student->department->id)
//            ->where('trimester_id',$request->trimester_id)
//            ->get();
//        return view('assign_subject.edit',compact('subjects','student'));
//    }
//
//    public function update(Request $request, $id)
//    {
////        $assign_subject = Subject::query()->findOrFail($id);
//        $assign_subject->update($request->all());
//        return redirect('assign_subject-manage');
//    }
//
//    public function destroy($id)
//    {
//        $assign_subject = Subject::query()->findOrFail($id);
//        $assign_subject->delete();
//        return redirect('assign_subject-manage');
//    }
}
