<?php

namespace App\Http\Controllers;

use App\Batch;
use App\Department;
use App\Evaluation;
use App\Mail\AdminMail;
use App\Question;
use App\Subject;
use App\SubjectTeacher;
use App\Teacher;
use App\Trimester;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{

    public function index()
    {
        $this->checkpermission('send-faculty-evaluation-result-search');
        $trimesters =Trimester::query()->pluck('name','id');
        $departments =Department::query()->pluck('name','id');
        return view('admin_mail.index',compact('trimesters','departments'));
    }


    public function send_faculty_evaluation_result(Request $request)
    {
//        dd($request->all());
        $teachers = Teacher::query()->where('department_id',$request->department_id)->get();
        $trimester_name = Trimester::query()->findOrFail($request->trimester_id)->name;
        foreach ($teachers as $teacher){
            $cum_avg =0;
            $subjects = Evaluation::query()
                ->where('teacher_id',$teacher->id)
                ->where('trimester_id',$request->trimester_id)
                ->groupBy('subject_id')
                ->get();

            $subject_count = $subjects->count();
            if($subject_count!=null){               //if teacher has evaluation
                foreach ($subjects as $subject) {
                    $sum = Evaluation::query()
                        ->where('subject_id',$subject->subject_id)
                        ->where('teacher_id',$teacher->id)
                        ->where('trimester_id',$request->trimester_id)
                        ->sum('marks');

                    $count = Evaluation::query()
                        ->where('subject_id',$subject->subject_id)
                        ->where('teacher_id',$teacher->id)
                        ->where('trimester_id',$request->trimester_id)
                        ->count();
                    $avg=$sum/$count;
                    $percentage = ($avg/6)*100;
                    $cum_avg+=$percentage;
                }
                $percentage = $cum_avg/$subject_count;
                $percentage= number_format($percentage,1);
                $email = $teacher->email;
                //Mail start
                Mail::send('admin_mail.send_faculty_evaluation_total_result', compact('teacher','trimester_name','percentage'), function($message) use ($email)  {
                    $message->to($email , $email)
                        ->subject('Trimester Faculty Evaluation Result');
                    $message->from('faisalnurroney@gmail.com','Faculty Evaluation Result');
                });
                //Mail ends
            }

        }

//        $html = "<table>
//                    <tr>
//                        <th>Subject Name</th>
//                        <th>Batch</th>
//                        <th>Percentage</th>
//                    </tr>
//                    ";
//        $trimesters =Trimester::query()->pluck('name','id');
//        $departments =Department::query()->pluck('name','id');
//        $teachers = Teacher::query()->where('department_id',$request->department_id)->get();
////        dd($teachers);
//        foreach ($teachers as $teacher) {
//            $subjects = SubjectTeacher::query()->where('teacher_id',$teacher->id)
//                ->groupBy('subject_id')->get();
//            foreach ($subjects as $subject){
//                $total = Evaluation::query()
//                    ->where('subject_id',$subject->subject_id)
//                    ->where('batch_id',$subject->batch_id)
//                    ->where('teacher_id',$teacher->id)
//                    ->sum('marks');
//                $count = Evaluation::query()
//                    ->where('subject_id',$subject->subject_id)
//                    ->where('batch_id',$subject->batch_id)
//                    ->where('teacher_id',$teacher->id)
//                    ->count();
//
//                $eva_number = $count*6;
//                $teacher = Teacher::query()->findOrFail($teacher->id);
//                if($count>0){
//                    $percentage = ($total/$eva_number)*100;
//
//                }
//                $subject_all = Subject::query()->findOrFail($subject->subject_id);
//                $batch = Batch::query()->findOrFail($subject->batch_id);
//                $trimester_all = Trimester::query()->findOrFail($request->trimester_id);
//
//
//                $email = $teacher->email;
//                //Mail start
//                Mail::send('admin_mail.send_faculty_evaluation_result', compact('html','teacher','trimester_all','subject_all','batch','percentage'), function($message) use ($email)  {
//                    $message->to($email , 'portcity.edu.bd')
//                        ->subject('Trimester Faculty Evaluation Result');
//                    $message->from('faisalnurroney@gmail.com','Faculty Evaluation Result');
//                });
//                //Mail ends
//            }
//        }

        $trimesters =Trimester::query()->pluck('name','id');
        $departments =Department::query()->pluck('name','id');
        return view('admin_mail.index',compact('trimesters','departments'));
    }
}

