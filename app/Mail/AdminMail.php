<?php

namespace App\Mail;

use App\Department;
use App\Trimester;
use Illuminate\Bus\Queueable;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AdminMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(Request $request)
    {
//        dd($request->all());
        return $this->view('admin_mail.send_faculty_evaluation_result');
//        return $this->view('admin_mail.send_faculty_evaluation_result',['msg'=>'hello','name'=>'faisal','subject'=>'demo subject'])->subject('demo_subject')->to('md.faisalnur55@gmail.com');
    }
}
