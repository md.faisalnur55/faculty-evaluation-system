<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evaluation extends Model
{
    protected $fillable = ['subject_id','batch_id','student_id','trimester_id','teacher_id','question_id','marks'];

    public function trimesters()
    {
        return $this->belongsTo(Trimester::class);
    }

    public function subjects()
    {
        return $this->belongsTo(Subject::class);
    }

    public function batches()
    {
        return $this->belongsTo(Batch::class);
    }

    public function students()
    {
        return $this->belongsTo(Student::class);
    }

    public function teachers()
    {
        return $this->belongsTo(Teacher::class);
    }

    public function questions()
    {
        return $this->belongsTo(Question::class);
    }
}
