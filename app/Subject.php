<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subject extends Model
{
    use SoftDeletes;
    protected $table='subjects';
    protected $fillable=['department_id','trimester_id','name','course_code','credit'];


    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    public function trimester()
    {
        return $this->belongsTo(Trimester::class);
    }

    public function students()
    {
        return $this->belongsToMany(Student::class);
    }

    public function teachers()
    {
        return $this->belongsToMany(Teacher::class,'subject_teacher');
    }
}
