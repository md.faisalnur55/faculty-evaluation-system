<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Student extends Model
{
    protected $table='students';
    protected $fillable=['department_id','batch_id','card_id','name','f_name','m_name','dob','email','phone'];

    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    public function batch()
    {
        return $this->belongsTo(Batch::class);
    }

    public function subjects()
    {
        return $this->belongsToMany(Subject::class);
    }

}
