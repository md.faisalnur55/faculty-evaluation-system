<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Route::get('/', function () {
//    return view('welcome');
//});
Route::group(['middleware' => 'auth'], function () {

    /** Dashboard Routes */
    Route::get('/','DashboardController@index');

    /** Role Routes */
    Route::get('role-manage','RoleController@index')->name('role.manage');
    Route::get('role-create','RoleController@create')->name('role.create');
    Route::post('role/store','RoleController@store')->name('role.store');
    Route::get('role-edit/{id}','RoleController@edit')->name('role.edit');
    Route::post('role/update/{id}','RoleController@update')->name('role.update');
    Route::delete('role-delete/{id}', 'RoleController@destroy')->name('role.delete');

    /** User Routes */
    Route::get('user-manage','UserController@index')->name('user.manage');
    Route::get('user-create','UserController@create')->name('user.create');
    Route::post('user/store','UserController@store');
    Route::get('user-edit/{id}','UserController@edit')->name('user.edit');
    Route::post('user/update/{id}','UserController@update')->name('user.update');
    Route::post('user-delete/{id}', 'UserController@destroy')->name('user.delete');
    /** Permission Routes */
//    Route::get('permission-manage','PermissionController@index');
//    Route::get('permission-create','PermissionController@create');
//    Route::post('permission/store','PermissionController@store');
//    Route::get('permission-edit/{id}','PermissionController@edit');
//    Route::post('permission/update/{id}','PermissionController@update');
//    Route::delete('permission-delete/{id}','PermissionController@destroy');

    /** Assign Permission Routes */
//    Route::get('permission-assign/{id}','PermissionController@assign_permission');
//    Route::post('permission/assign/update/{id}','PermissionController@assign_permission_update');

    /** Department Setting Routes */
    Route::get('department-manage','DepartmentController@index')->name('department.manage');
    Route::get('department-create','DepartmentController@create')->name('department.create');
    Route::post('department/store','DepartmentController@store');
    Route::get('department-edit/{id}','DepartmentController@edit')->name('department.edit');
    Route::post('department/update/{id}','DepartmentController@update');
    Route::delete('department-delete/{id}', 'DepartmentController@destroy')->name('department.delete');

    /** Trimester Setting Routes */
    Route::get('trimester-manage','TrimesterController@index')->name('trimester.manage');
    Route::get('trimester-create','TrimesterController@create')->name('trimester.create');
    Route::post('trimester/store','TrimesterController@store');
    Route::get('trimester-edit/{id}','TrimesterController@edit')->name('trimester.edit');
    Route::post('trimester/update/{id}','TrimesterController@update');
    Route::delete('trimester-delete/{id}', 'TrimesterController@destroy')->name('trimester.delete');


    /** Subject Setting Routes */
    Route::get('subject-manage','SubjectController@index')->name('subject.manage');
    Route::get('subject-create','SubjectController@create')->name('subject.create');
    Route::post('subject/store','SubjectController@store');
    Route::get('subject-edit/{id}','SubjectController@edit')->name('subject.edit');
    Route::post('subject/update/{id}','SubjectController@update');
    Route::delete('subject-delete/{id}', 'SubjectController@destroy')->name('subject.delte');

    /** Question Setting Routes */
    Route::get('question-manage','QuestionController@index')->name('question.manage');
    Route::get('question-create','QuestionController@create')->name('question.create');
    Route::post('question/store','QuestionController@store');
    Route::get('question-edit/{id}','QuestionController@edit')->name('question.edit');
    Route::post('question/update/{id}','QuestionController@update');
    Route::delete('question-delete/{id}', 'QuestionController@destroy')->name('question.delete');


    /** Teacher Setting Routes */
    Route::get('teacher-manage','TeacherController@index')->name('teacher.manage');
    Route::get('teacher-create','TeacherController@create')->name('teacher.create');
    Route::post('teacher/store','TeacherController@store');
    Route::get('teacher-edit/{id}','TeacherController@edit')->name('teacher.edit');
    Route::post('teacher/update/{id}','TeacherController@update');
    Route::delete('teacher-delete/{id}', 'TeacherController@destroy')->name('teacher.delete');

    /** Student Setting Routes */
    Route::get('student-manage','StudentController@index')->name('student.manage');
    Route::get('student-create','StudentController@create')->name('student.create');
    Route::post('student/store','StudentController@store');
    Route::get('student-edit/{id}','StudentController@edit')->name('student.edit');
    Route::post('student/update/{id}','StudentController@update');
    Route::delete('student-delete/{id}', 'StudentController@destroy')->name('student.delete');


    /** Batch Setting Routes */
    Route::get('batch-manage','BatchController@index')->name('batch.manage');
    Route::get('batch-create','BatchController@create')->name('batch.create');
    Route::post('batch/store','BatchController@store');
    Route::get('batch-edit/{id}','BatchController@edit')->name('batch.edit');
    Route::post('batch/update/{id}','BatchController@update');
    Route::delete('batch-delete/{id}', 'BatchController@destroy')->name('batch.delete');


    /** Teacher Evaluation Routes */
    Route::get('evaluation-manage','EvaluationController@index')->name('evaluation.manage');
    Route::get('evaluation-create','EvaluationController@create')->name('evaluation.create');
    Route::get('evaluation-search','EvaluationController@search')->name('evaluation.search');
    Route::post ('evaluation-view','EvaluationController@view')->name('evaluation.view');
    Route::post('evaluation/store','EvaluationController@store');
    Route::get('evaluation-edit/{id}','EvaluationController@edit')->name('evaluation.edit');
    Route::post('evaluation/update/{id}','EvaluationController@update');
    Route::delete('evaluation-delete/{id}', 'EvaluationController@destroy')->name('evaluation.delete');
    Route::get('evaluation-total-search','EvaluationController@total_search')->name('evaluation.search.total');
    Route::post('evaluation-total-view','EvaluationController@total_evaluation_view')->name('evaluation.view.total');

    /** Assign Subject To Student Routes */
    Route::get('assign-subject-manage','AssignSubjectController@index')->name('assign_subject.manage');
    Route::post('assign-subject-create','AssignSubjectController@create')->name('assign_subject.create');
    Route::post('assign-subject/store','AssignSubjectController@store');
    Route::get('assign-subject-edit/{id}','AssignSubjectController@edit')->name('assign_subject.edit');
    Route::post('assign-subject/update/{id}','AssignSubjectController@update');
    Route::delete('assign-subject-delete/{id}', 'AssignSubjectController@destroy')->name('assign_subject.delete');

    /** Assign Subject To Teacher Routes */
    Route::get('assign-subject-teacher-manage','AssignSubjectTeacherController@index')->name('assign_subject_teacher.manage');
    Route::get('assign-subject-teacher-view','AssignSubjectTeacherController@show');
    Route::post('assign-subject-teacher-create','AssignSubjectTeacherController@create')->name('assign_subject_teacher.create');
    Route::post('assign-subject-teacher/store','AssignSubjectTeacherController@store');
    Route::get('assign-subject-teacher-edit/{id}','AssignSubjectTeacherController@edit')->name('assign_subject_teacher.edit');
    Route::post('assign-subject-teacher/update/{id}','AssignSubjectTeacherController@update');
    Route::delete('assign-subject-teacher-delete/{id}', 'AssignSubjectTeacherController@destroy')->name('assign_subject_teacher.delete');

    /** File Upload Routes */
    Route::get('file-upload-manage','FileUploadController@index')->name('file_upload.manage');
    Route::get('file-upload-create','FileUploadController@create')->name('file_upload.create');
    Route::post('file-upload/store','FileUploadController@store');
    Route::get('file-upload-edit/{id}','FileUploadController@edit')->name('file_upload.edit');
    Route::post('file-upload/update/{id}','FileUploadController@update');
    Route::delete('file-upload-delete/{id}', 'FileUploadController@destroy')->name('file_upload.delete');

    /** Course Material Routes */
    Route::post('course-material-manage','CourseMaterialController@index')->name('course.manage');
    Route::get('course-material-search','CourseMaterialController@search')->name('course.search');

    /** Faculty Result Mail*/
    Route::get('send-faculty-evaluation-result-search','MailController@index')->name('send_email.create');
    Route::post('send-faculty-evaluation-result','MailController@send_faculty_evaluation_result')->name('send.faculty');
});


//Ajax routes
Route::post('load_student_card_id','StudentController@loadStudentCardId')->name('load_student_card_id');
Route::post('load_teacher','AssignSubjectTeacherController@load_teacher')->name('load_teacher');
Route::post('load_evaluation_teacher','EvaluationController@load_teacher')->name('load_evaluation_teacher');
Route::post('load_subject','EvaluationController@load_subject')->name('load_subject');
Route::post('load_batch','EvaluationController@load_batch')->name('load_batch');
Route::post('load_batches','FileUploadController@load_batches')->name('load_batches');
Route::post('load_subjects','FileUploadController@load_subjects')->name('load_subjects');
Route::post('load_course_material_subjects','CourseMaterialController@load_course_material_subjects')->name('load_course_material_subjects');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');